CC = g++
CFLAGS = -Wall -I./src -g
LDFLAGS = -lncurses -ltinfo -lconfig++

YARG_SRC = src/yarg.cpp \
	src/Level.cpp \
	src/Player.cpp \
	src/Interface.cpp \
	src/GameSystem.cpp \
	src/Enemy.cpp\
	 src/Dijkstra.cpp \
	src/ConfigParser.cpp \
	src/Character.cpp \
	src/Visibility.cpp \
	src/Utils.cpp \
	src/Frame.cpp \
	src/Screen.cpp \
	src/getchar.cpp\
	src/MapGenerator.cpp\
	src/GameClock.cpp

YARG_OBJ = build/yarg.o \
	build/Level.o \
	build/Player.o \
	build/Interface.o \
	build/GameSystem.o \
	build/Enemy.o \
	build/Dijkstra.o \
	build/ConfigParser.o \
	build/Character.o \
	build/Visibility.o \
	build/Utils.o \
	build/Frame.o \
	build/Screen.o \
	build/getchar.o\
	build/MapGenerator.o\
	build/GameClock.o

YARG_EXECUTABLE = yarg

MAP_GENERATOR_SRC = src/MapGenerator.cpp \
			src/MapTest.cpp\
			src/ConfigParser.cpp\
			src/Dijkstra.cpp

MAP_GENERATOR_OBJ = build/MapGenerator.o \
			build/MapTest.o \
			build/ConfigParser.o\
			build/Dijkstra.o

MAP_GENERATOR_EXECUTABLE = MapGenerator

.PHONY: all clean

all: yarg MapGenerator

debug: CFLAGS += -DDEBUG
debug: all

build:
	mkdir -p build

yarg: $(YARG_OBJ)| build
	$(CC) $(LDFLAGS) $^ -o $@

MapGenerator: $(MAP_GENERATOR_OBJ) | build
	$(CC) $(LDFLAGS) $^ -o $@

build/%.o: src/%.cpp | build
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -rf build yarg MapGenerator
