# YARG- Yet another roguelike game

Simple roguelike-engine written from scratch to learn C++

## Features
  * Maps
     -  define a map rotation consisting of either procedurally generated dungeons at  
        runtime and/or load handcrafted maps (defined by text files)
     - configurable tiles: blocks movement, translucent, movement cost, battle modifiers, color, description
  * ASCII character set for monsters and map freely configurable, including all properties used by the engine
  * field of view: you see only what your character can see in the dungeon
  * moving map with for of war
  * flavor texts for atmosphere: descriptions for all map tiles, enemies, objects, battle cries and so on, also freely configurable
  * tactical fights: use map to your advantage (hide from enemies, use battle modifiers of tiles)
  * simple time systems: 
     - monsters can be faster or slower than player, 
     - actions consume action points, dependent on the movement cost
     - player-independent game clock
    defined for each tile
  * Basic Monster AI
	   - Monsters adhere to Field of View based on level geometry
	   - pathfinding with Dijkstra algorithm (currently used when player within FoV)
	   - otherwise random movement
  * colored output ("colored" in the ncurses sense)

<img src="showcase.gif" width="750" height="600" />

## Compiling/ running
After you checked out the repository, run `make` to create the executable (run
`make debug` to do so with debugging information).
Start the game by entering `./yarg` in the terminal.

## Requirements/ Dependencies
In order to compile yarg, you need to have the `ncurses` and `libconfig` library installed.

Originally, yarg is based on the code and principles shown in this youtube video:
https://www.youtube.com/watch?v=tVWckBaB5xo
(MakingGamesWithBen "Game Challenge 3: ASCII Roguelike!"). From there on, it got
extended heavily and is ever growing still.


(c) 2023 Denis Zetzmann, d1z@gmx.de
