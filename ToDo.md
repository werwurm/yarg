# ToDo List for yarg
## bugs
  * enemy movement: consider field with another enemy as blocked- so enemies do not line up to fight player but look for a new attack position
  * deallocation issues- see valgrind output
  * code refactoring: code styleguide for methods and members

## To be implemented
  * add monster placement algorithm
  * add different mechanics/ map layers: scent, sound
  * Zombies!
  * limit player vision
    - (light system? (torches, fireballs, ...)
  * procedural dungeon
  * player, monster and NPC chat with each other
  * localisation support


## Nice to have
  * light emitting tiles
  * equipment system
  * parties of more than one player


## Idea brainstorm
  * saving/ restoring game state to resume games, keeping permadeath
  * openGL graphics --> raylib
  * objects in world that have actions/behaviour attached (e.g. apple that tells player "I am eatable and will restore health")

## Potential for Optimization
  * check wich parameters can/should be made const & to avoid unnecessary copying
  * character FoV doesn't need to contain full map, only _position +- viewing distance  
    --> FoV map gets fully nullified and then redrawn each turn, for each active character
  * similarly: Dijkstra pathfinding currently uses full map, mainly used when player gets within FoV of Monsters: add a fast lane and try Dijkstra on FoV first
