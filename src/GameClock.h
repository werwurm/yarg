#pragma once

#include "TimedEntities.h"
#include "Datatypes.h"

class GameClock : public TimedEntity {
public:
    GameClock() : TimedEntity(100) {}

    std::string getTimeString() const;
    uint getNrRounds() const;
    virtual int takeTurn(EntityAction action = EntityAction::IDLE) override;
};


