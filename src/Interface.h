#pragma once
#include <ncurses.h>
#include "Frame.h"
#include "Debug.h"
#include "Datatypes.h"

#define MAX_MESSAGES 6          // number of max game log messages displayed
#define MESSAGE_LENGTH 50

#ifdef DEBUG_CONSOLE
    #define MAX_DEBUG_MSG 16
    #define DEBUG_MSG_LENGTH 50
#endif

// shortcuts for game log, variadic macros that write to gameLog window
#define PLAYER_HEARS(fmt, ...) addGameLog("You hear " fmt, ##__VA_ARGS__)
#define PLAYER_SEES(fmt, ...)  addGameLog("You see " fmt, ##__VA_ARGS__)

class Interface
{
public:
    Interface();
    void init(MapSize mapSize);
    Frame map;          // complete game Map
    Frame mapWin;       // map Window of the interface
    Frame commandWin;   // where player enters commands
    Frame gameLog;      // where all game messages are printed
    Frame playerStatus; // status window for player
    Frame enemyStatus;  // status window for enemy
    
    #ifdef DEBUG_CONSOLE
        Frame debugWin;     // create a debug log window when running with debug symbols
    #endif

    void refreshAll();         // redraw all interface windows
    void refreshComponent(Frame &interfaceComp);    // redraw specific window
    void addMsg(Frame &targetFrame, const char* format, ...) ; // add a text message to a specific window
    void addGameLog(char const *fmt, ...);                 // add a new message to scrolling gameLog
    void updatePlayerStatus(int level, int health, int attack, int defence, int experience);
    void updateEnemyStatus(char const *name, int level, int attack, int defense, int health);
    
    #ifdef DEBUG_CONSOLE
        void addDebugMsg(char const *fmt, ...);                 // add a new message to scrolling debug win
    #endif
        
    void clearWin(Frame &targetFrame);      // clear specific window
    void clearAll();                        // clear all windows

private:
    char *m_gameLogMsgs[MAX_MESSAGES];    // stores the last game log gameLogMsg
    int m_messageIndex;                   // current last message in gameLogsMsgs
    
    #ifdef DEBUG_CONSOLE
        char *m_debugMsgs[MAX_DEBUG_MSG];
        int m_debugMsgIdx;
    #endif
};
