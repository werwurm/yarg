#include "Frame.h"

Frame::Frame(){
}

// Initialize a main window (no parent)
void Frame::init(int nrm_rows, int nrm_cols, int row_0, int col_0) {
	m_hasParent = FALSE;
	m_parentWindow = NULL;
	m_window = newwin(nrm_rows, nrm_cols, row_0, col_0);
	m_height = nrm_rows;
	m_width = nrm_cols;
	m_row = row_0;
	m_col = col_0;
}

// Initialize a subwindow (viewport) with a parent window
void Frame::init(Frame &sw, int nrm_rows, int nrm_cols, int row_0, int col_0) {
	m_hasParent = TRUE;
	m_parentWindow = sw.getWin();
	m_window = derwin(sw.getWin(), nrm_rows, nrm_cols, row_0, col_0);
	m_height = nrm_rows;
	m_width = nrm_cols;
	m_row = row_0;
	m_col = col_0;
}

Frame::~Frame() {
	delwin(m_window);
}

// Center the map viewport around the player
void Frame::center(int playerX, int playerY) {
    // Check if the Frame has a parent window
    if(m_hasParent) {
        // Initialize variables for the current row, column, and dimensions of the Frame
        int currm_row = m_row, currm_col = m_col, height, width;

        // Calculate the new row and column values to center the Frame around the player
        int newm_row = playerY - m_height/2;
        int newm_col = playerX - m_width/2;

        // Get the dimensions of the parent window
        getmaxyx(m_parentWindow, height, width);

        // Check if the Frame goes off the right side of the parent window
        if(newm_col + m_width >= width) {
            // Calculate the amount the Frame goes off the right side of the parent window
            int delta = width - (newm_col + m_width);
            // Set the new column to the right-most position within the parent window
            currm_col = newm_col + delta;
        }
        else {
            // Set the new column to the calculated value
            currm_col = newm_col;
        }

        // Check if the Frame goes off the bottom of the parent window
        if(newm_row + m_height >= height) {
            // Calculate the amount the Frame goes off the bottom of the parent window
            int delta = height - (newm_row + m_height);
            // Set the new row to the bottom-most position within the parent window
            currm_row = newm_row + delta;
        }
        else {
            // Set the new row to the calculated value
            currm_row = newm_row;
        }

        // Check if the Frame goes off the top of the parent window
        if (newm_row < 0) {
            // Set the new row to the top-most position within the parent window
            currm_row = 0;
        }

        // Check if the Frame goes off the left side of the parent window
        if (newm_col < 0) {
            // Set the new column to the left-most position within the parent window
            currm_col = 0;
        }
        // Move the cursor to the new row and column positions
        move(currm_row, currm_col);
    }
}

// Refresh the window
void Frame::refresh() const{
	if(m_hasParent) {
		touchwin(m_parentWindow);
	}
	wrefresh(m_window);
}

// Move a window in a new position (r, c)
void Frame::move(int r, int c) {
	if(m_hasParent) {
		mvderwin(m_window, r, c);
		m_row = r;
		m_col = c;
		refresh();
	}
}

// Get the window
WINDOW* Frame::getWin() const{
	return m_window;
}

// Get the parent window
WINDOW* Frame::getParentWin() const{
	return m_parentWindow;
}

// Get window type, if TRUE we have a subwindow, if FALSE we have a main window
bool Frame::hasParent() const{
	return m_hasParent;
}

// Get the height of the window
int Frame::getHeight() const{
	return m_height;
}

// Get the width of the window
int Frame::getWidth() const{
	return m_width;
}

// Get the row (y) position of the window
int Frame::row() const{
	return m_row;
}

// Get the row (y) position of the window
int Frame::col() const{
	return m_col;
}
