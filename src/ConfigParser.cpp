#include <libconfig.h++>
#include <iostream>
#include <string>
#include "ConfigParser.h"

 
// Read enemy configuration file and store data in a vector
std::vector<EnemyData> cfg_readEnemiesConfig(const std::string& configFile) {
    std::vector<EnemyData> enemiesData;
	
    try {
		libconfig::Config config;
		config.readFile(configFile.c_str());

		const libconfig::Setting& enemiesSetting = config.getRoot()["enemies"];

		for (int i = 0; i < enemiesSetting.getLength(); ++i) {
			const libconfig::Setting& enemySetting = enemiesSetting[i];

			char tile;
			std::string name;
			uint32_t level;
			int health;
			uint32_t attack;
			uint32_t defense;
			uint32_t experienceValue;
			uint32_t visRange = 5 ;		// default value = 5
			std::vector<std::string> descriptions;
			std::vector<std::string> battleCries;
			int speed = 100;

			// Retrieve values from config using lookupValue() and convert to char
			if (enemySetting.exists("tile")) {
				std::string tileStr;
				enemySetting.lookupValue("tile", tileStr);
				if (!tileStr.empty()) {
					tile = tileStr[0];
				}
			}

			enemySetting.lookupValue("name", name);
			enemySetting.lookupValue("level", level);
			enemySetting.lookupValue("health", health);
			enemySetting.lookupValue("attack", attack);
			enemySetting.lookupValue("defense", defense);
			enemySetting.lookupValue("experienceValue", experienceValue);
			enemySetting.lookupValue("visibilityRange", visRange);
			enemySetting.lookupValue("speed", speed);
			
			const libconfig::Setting& descriptionsSetting = enemySetting.lookup("descriptions");
			for (int j = 0; j < descriptionsSetting.getLength(); ++j) {
				std::string description;
				description = descriptionsSetting[j].c_str();
				descriptions.push_back(description);
			}
			
			const libconfig::Setting& battleCriesSetting = enemySetting.lookup("battleCries");
			for (int j = 0; j < battleCriesSetting.getLength(); ++j) {
				std::string battleCry;
				battleCry = battleCriesSetting[j].c_str();
				battleCries.push_back(battleCry);
			}

			EnemyData enemyData;
			enemyData.tile = tile;
			enemyData.name = name;
			enemyData.level = level;
			enemyData.health = health;
			enemyData.attack = attack;
			enemyData.defense = defense;
			enemyData.experienceValue = experienceValue;
			enemyData.visibilityRange = visRange;
			enemyData.speed = speed;
			enemyData.descriptions = descriptions;
			enemyData.battleCries = battleCries;
			
			enemiesData.push_back(enemyData);
		}
	} catch (const libconfig::FileIOException& e) {
        std::cerr << "Error reading enemies configuration file: " << e.what() << std::endl;
        exit(0);
    } catch (const libconfig::ParseException& e) {
        std::cerr << "Error parsing enemies configuration file: " << e.getFile() << ":" << e.getLine() << " - " << e.getError() << std::endl;
        exit(0);
	}
    return enemiesData;
}

// checks if a given tile represents an enemy in the enemies list
// returns -1 if not in list or the position within the list
int cfg_EnemiesDataPos(char tile, const std::vector<EnemyData>* enemies) {
    if (enemies == nullptr) {
        return -1; // Handle null pointer case
    }
    for (size_t i = 0; i < enemies->size(); i++) {
        if ((*enemies)[i].tile == tile) {
            return static_cast<int>(i); // Return position as signed integer
        }
    }
    return -1; // Enemy not found
}

// Debug routine, printout parsed enemy config file
void cfg_printEnemiesData(const std::vector<EnemyData>* enemiesDataPtr) {
    if (enemiesDataPtr == nullptr) {
        std::cout << "Error: nullptr passed for enemiesDataPtr." << std::endl;
        return;
    }

    const std::vector<EnemyData>& enemiesData = *enemiesDataPtr;

    // Print out the content of the enemiesData vector
    std::cout << "Enemies Data:" << std::endl;
    for (const EnemyData& enemy : enemiesData) {
        std::cout << "Tile: " << enemy.tile << std::endl;
        std::cout << "Name: " << enemy.name << std::endl;
        std::cout << "Level: " << enemy.level << std::endl;
        std::cout << "Health: " << enemy.health << std::endl;
        std::cout << "Attack: " << enemy.attack << std::endl;
        std::cout << "Defense: " << enemy.defense << std::endl;
        std::cout << "XP: " << enemy.experienceValue << std::endl;
        std::cout << "Visibility Range: " << enemy.visibilityRange << std::endl;
        std::cout << "Speed: " << enemy.speed << std::endl;
        
        std::cout << "Descriptions: ";
        for (const std::string& desc : enemy.descriptions) {
            std::cout << desc << " ";
        }
        std::cout << "Battle Cries: ";
        for (const std::string& cries : enemy.battleCries) {
            std::cout << cries << " ";
        }
        std::cout << std::endl;
    }
}


// Read map configuration file and store data in a vector
std::vector<MapData> cfg_readMapConfig(const std::string& configFile) {
    std::vector<MapData> mapData;
	
    try {
		libconfig::Config config;
		config.readFile(configFile.c_str());

		const libconfig::Setting& mapSettings = config.getRoot()["map_tiles"];

		for (int i = 0; i < mapSettings.getLength(); ++i) {
			const libconfig::Setting& curSetting = mapSettings[i];

			char tile = '\0'; 
			bool blocks = false;         
			bool translucent = true;  
			uint movementCost = 100; 
			std::string color;
			std::string name;
			std::vector<std::string> descriptions;

			// Retrieve values from config using lookupValue() and convert to char
			if (curSetting.exists("tile")) {
				std::string tileStr;
				curSetting.lookupValue("tile", tileStr);
				if (!tileStr.empty()) {
					tile = tileStr[0];
				}
			}
			
			curSetting.lookupValue("name", name);
			curSetting.lookupValue("blocks", blocks);
			curSetting.lookupValue("translucent", translucent);
			curSetting.lookupValue("movementCost", movementCost);
			curSetting.lookupValue("color", color);

			const libconfig::Setting& descriptionsSetting = curSetting.lookup("descriptions");
			
			// Generate a random index within the range of valid indices for the vector
			srand(static_cast<unsigned int>(time(0))); 			// Seed the random number generator with the current time
			int randomIndex = rand() % descriptionsSetting.getLength();
			//Select a random description based on the random index
			std::string randomDescription = descriptionsSetting[randomIndex];
			
			// check if this tile has a "battleModifiers" section, read in modifiers if so
			int attackModifier = 0, defenceModifier = 0;
			if(curSetting.exists("battleModifiers")) {		
				const libconfig::Setting& battleModSettings = curSetting["battleModifiers"];
				if (battleModSettings.exists("attackModifier")) {
					attackModifier = battleModSettings["attackModifier"];
				}
				if (battleModSettings.exists("defenceModifier")) {
					defenceModifier = battleModSettings["defenceModifier"];
				}
			}
				
			// fill struct with parsed values
			MapData tileData;
			tileData.tile = tile;
			tileData.name = name;
			tileData.blocks = blocks;
			tileData.translucent = translucent;
			tileData.movementCost = movementCost;
			tileData.color = color;
			tileData.description = randomDescription;
			tileData.attackModifier = 1.0f + attackModifier / 100.0f;
			tileData.defenceModifier = 1.0f + defenceModifier / 100.0f;
			mapData.push_back(tileData);	// add to mapData vector
		}
	} catch (const libconfig::FileIOException& e) {
        std::cerr << "Error reading map configuration file: " << e.what() << std::endl;
    } catch (const libconfig::ParseException& e) {
        std::cerr << "Error parsing map configuration file: " << e.getFile() << ":" << e.getLine() << " - " << e.getError() << std::endl;
	}
    return mapData;
}

// checks if a given tile was configured and is in MapData vector
// returns -1 if not in list or the position within the list
int cfg_MapTileDataPos(char tile, const std::vector<MapData>* tilesDataPtr) {
    if (tilesDataPtr == nullptr) {
        return -1; // Handle null pointer case
    }
    for (size_t i = 0; i < tilesDataPtr->size(); i++) {
        if ((*tilesDataPtr)[i].tile == tile) {
            return static_cast<int>(i); // Return position as signed integer
        }
    }
    return -1; // mapTile not found (so it is not defined in Map config file)
}

// returns map tile character that belongs to a map tile name
char cfg_getMapTileByName(const std::vector<MapData>& mapData, const std::string& name){
	for (const MapData& data : mapData) {
        if (data.name == name) {
            return data.tile;
        }
    }
    return '?';
}

// Debug routine, printout parsed map config file
void cfg_printMapData(const std::vector<MapData>* tilesDataPtr) {
    if (tilesDataPtr == nullptr) {
        std::cout << "Error: nullptr passed for tilesDataPtr." << std::endl;
        return;
    }

    const std::vector<MapData>& tilesData = *tilesDataPtr;

    // Print out the content of the mapData vector
    std::cout << "Tiles Data:" << std::endl;
    for (const MapData& tile : tilesData) {
        std::cout << "Tile: " << tile.tile << std::endl;
        std::cout << "blocks movement: " << tile.blocks << std::endl;
        std::cout << "translucent: " << tile.translucent << std::endl;
        std::cout << "movementCost: " << tile.movementCost << std::endl;
        std::cout << "Color: " << tile.color << std::endl;
        std::cout << "Descriptions: " << tile.description << std::endl;
        std::cout << std::endl;
    }
}

// read game config and store data in struct
GameConfig cfg_readGameConfig(const std::string& configFile) {

    GameConfig gameConfig;

    try {
        libconfig::Config config;
        config.readFile(configFile.c_str());

        const libconfig::Setting& configSettings = config.getRoot()["config_files"];

        // Read MapConfigFile
        if (configSettings.exists("MapConfigFile")) {
            const std::string& mapConfigFile = configSettings["MapConfigFile"];
            gameConfig.MapConfigFile = mapConfigFile;
        }

        // Read EnemyConfigFile
        if (configSettings.exists("EnemyConfigFile")) {
            const std::string& enemyConfigFile = configSettings["EnemyConfigFile"];
            gameConfig.EnemyConfigFile = enemyConfigFile;
        }
        
        // read level configuration
        const libconfig::Setting& levelSettings = config.getRoot()["level_config"];
		const libconfig::Setting& levelNamesSetting = levelSettings.lookup("levelNames");
        
        std::vector<std::string> levelNames;
        uint nrLevels = 0;      
		for (int j = 0; j < levelNamesSetting.getLength(); ++j) {
				std::string levelName;
				levelName = levelNamesSetting[j].c_str();
				levelNames.push_back(levelName);
				nrLevels++;
			}
		gameConfig.nrLevels = nrLevels;
		gameConfig.levelNames = levelNames;
        
        // read cheat modes
        const libconfig::Setting& cheatSettings = config.getRoot()["cheat_modes"];
        if (cheatSettings.exists("MindVision")) {
			bool mindVision = cheatSettings["MindVision"];
			gameConfig.mindVision = mindVision;
		}
		if (cheatSettings.exists("PlayerFoV")) {
			bool playerFoV = cheatSettings["PlayerFoV"];
			gameConfig.playerFoV = playerFoV;
		}
		// read Map Generator Config
		const libconfig::Setting& mapGenSettings = config.getRoot()["map_generator"];
		if(mapGenSettings.exists("seed")) {
			uint seed = mapGenSettings["seed"];
			gameConfig.seed = seed;
		}
		if(mapGenSettings.exists("mapWidth")) {
			uint mapWidth = mapGenSettings["mapWidth"];
			gameConfig.mapWidth = mapWidth;
		}
		if(mapGenSettings.exists("mapHeight")) {
			uint mapHeight = mapGenSettings["mapHeight"];
			gameConfig.mapHeight = mapHeight;
		}
		const libconfig::Setting& CellAutSettings = mapGenSettings["CellularAutomata"];
		if(CellAutSettings.exists("iterations")) {
			uint CellAutIterations = CellAutSettings["iterations"];
			gameConfig.CellAutIterations = CellAutIterations;
		}
		if(CellAutSettings.exists("percentWalls")) {
			uint CellAutPercentWalls = CellAutSettings["percentWalls"];
			gameConfig.CellAutPercentWalls = CellAutPercentWalls;
		}
		const libconfig::Setting& DrunkardSettings = mapGenSettings["DrunkardsWalk"];
		if(DrunkardSettings.exists("pathLength")){
			uint DrunkardPathLength = DrunkardSettings["pathLength"];
			gameConfig.DrunkardPathLength = DrunkardPathLength;
		}
		if(DrunkardSettings.exists("percentWalls")){
			uint DrunkardPercentWalls = DrunkardSettings["percentWalls"];
			gameConfig.DrunkardPercentWalls = DrunkardPercentWalls;
		}		
		if(DrunkardSettings.exists("startCenter")){
			bool DrunkardStartCenter= DrunkardSettings["startCenter"];
			gameConfig.DrunkardStartCenter = DrunkardStartCenter;
		}
		
		const libconfig::Setting& PostProcSettings = mapGenSettings["PostProcessing"];
		if(PostProcSettings.exists("nrLakes")) {
			uint nrLakes = PostProcSettings["nrLakes"];
			gameConfig.nrLakes = nrLakes;
		}
		if(PostProcSettings.exists("minLakeSize")) {
			uint minLakeSize = PostProcSettings["minLakeSize"];
			gameConfig.minLakeSize = minLakeSize;
		}
		if(PostProcSettings.exists("maxLakeSize")) {
			uint maxLakeSize = PostProcSettings["maxLakeSize"];
			gameConfig.maxLakeSize = maxLakeSize;
		}	
		if(PostProcSettings.exists("mossNextWaterProb")) {
			uint mossNextWaterProb = PostProcSettings["mossNextWaterProb"];
			gameConfig.mossNextWaterProb = mossNextWaterProb;
		}			
		if(PostProcSettings.exists("mossNextWallProb")) {
			uint mossNextWallProb = PostProcSettings["mossNextWallProb"];
			gameConfig.mossNextWallProb = mossNextWallProb;
		}			
		if(PostProcSettings.exists("mossNextWaterWallProb")) {
			uint mossNextWaterWallProb = PostProcSettings["mossNextWaterWallProb"];
			gameConfig.mossNextWaterWallProb = mossNextWaterWallProb;
		}
		if(PostProcSettings.exists("treeProb")) {
			uint treeProb = PostProcSettings["treeProb"];
			gameConfig.treeProb = treeProb;
		}			
		if(PostProcSettings.exists("treeNeighbourProb")) {
			uint treeNeighbourProb = PostProcSettings["treeNeighbourProb"];
			gameConfig.treeNeighbourProb = treeNeighbourProb;
		}
		if(PostProcSettings.exists("gateWallProb")) {
			uint gateWallProb = PostProcSettings["gateWallProb"];
			gameConfig.gateWallProb = gateWallProb;
		}			
		if(PostProcSettings.exists("gateWaterProb")) {
			uint gateWaterProb = PostProcSettings["gateWaterProb"];
			gameConfig.gateWaterProb = gateWaterProb;
		}	
		
    } catch (const libconfig::FileIOException& e) {
        std::cerr << "Error reading game configuration file: " << e.what() << std::endl;
    } catch (const libconfig::ParseException& e) {
        std::cerr << "Error parsing game configuration file: " << e.getFile() << ":" << e.getLine() << " - " << e.getError() << std::endl;
    }

    return gameConfig;
}

// debug routine, printout parsed config
void cfg_printConfigData(const GameConfig& configPtr) {
	std::cout << "------------ Config Files ----------------" << std::endl;
	std::cout << "Map Config File: " << configPtr.MapConfigFile << std:: endl;
	std::cout << "Enemy Config File: " << configPtr.EnemyConfigFile << std::endl;
	std::cout << "------------ Level Configuration ---------" << std::endl;
	std::cout << "Nr. of Levels: " << configPtr.nrLevels << std::endl;
	std::cout << "Level Names: " << std::endl;
	for (const std::string& levelName : configPtr.levelNames) {
            std::cout << levelName << " ";
        }
	std::cout << "\n------------ Cheat Modes -----------------" << std::endl;
	std::cout << "Mind Vision: " << configPtr.mindVision << std::endl;
	std::cout << "Player FoV: " << configPtr.playerFoV << std::endl;
	std::cout << "------------ Map Generator ---------------" << std::endl;
	std::cout << "Seed: " << configPtr.seed << std::endl;
	std::cout << "Map Width: " << configPtr.mapWidth << std::endl;
	std::cout << "Map Height: " << configPtr.mapHeight << std::endl;
	std::cout << "---------\nCellAut Iterations: " << configPtr.CellAutIterations << std::endl;
	std::cout << "CellAut PercentWalls: " << configPtr.CellAutPercentWalls << std::endl;
	std::cout << "---------\nDrunkWalk pathLength: " << configPtr.DrunkardPathLength << std::endl;
	std::cout << "DrunkWalk PercentWalls: " << configPtr.DrunkardPercentWalls << std::endl;
	std::cout << "DrunkWalk startCenter: " << configPtr.DrunkardStartCenter << std::endl;
	std::cout << "---------\nNr of Lakes: " << configPtr.nrLakes << std::endl;
	std::cout << "min Lake Size: " << configPtr.minLakeSize << std::endl;
	std::cout << "max Lake Size: " << configPtr.maxLakeSize << std::endl;
	std::cout << "---------\nProb. of moss near water: " << configPtr.mossNextWaterProb << std::endl;
	std::cout << "Prob. of moss near walls: " << configPtr.mossNextWallProb << std::endl;
	std::cout << "Prob. of moss near water AND walls: " << configPtr.mossNextWaterWallProb << std::endl;
	std::cout << "---------\nProb. of a tree in a free 3x3 area: " << configPtr.treeProb << std::endl;
	std::cout << "Prob. of trees having neighbour trees: " << configPtr.treeNeighbourProb << std::endl;
	std::cout << "---------\nP for a wall (surrounded by floor/ gate) to be replaced by gate " << configPtr.gateWallProb << std::endl;
	std::cout << "P for wall tile next to water to be replaced by gate " << configPtr.gateWaterProb << std::endl;
}
	
