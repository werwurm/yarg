#pragma once
#include <deque>

#include "Datatypes.h"

class TimedEntity {
public:
    TimedEntity(int speed) : m_speed(speed){}
    virtual ~TimedEntity() {}
	
	int getActionPoints() const { return m_actionPoints;}
	int getSpeed() const {return m_speed;}
	
	void setActionPoints(int ap) { m_actionPoints = ap;}
	void setSpeed(int speed) { m_speed = speed;}
	
    void registerWithTimekeeper(std::deque<TimedEntity*>& timedEntities) {       
        timedEntities.push_back(this);
        m_actionPoints = 0;
	}

    void removeFromTimekeeper(std::deque<TimedEntity*>& timedEntities) {
        auto it = std::find(timedEntities.begin(), timedEntities.end(), this);
        if (it != timedEntities.end()) {
            timedEntities.erase(it);
        }
    }

    void tick() {
        m_actionPoints += m_speed;
    }

    virtual int takeTurn(EntityAction action = EntityAction::IDLE) = 0;

private:
    int m_speed;
    int m_actionPoints;
};
