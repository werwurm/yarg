#pragma once

#include <vector>
#include <queue>
#include <limits>
#include <functional>

#include "Datatypes.h"

// Class that provides pathfinding in the game map
class Dijkstra {
public:
    Dijkstra();
    std::vector<LevelPoint> FindPath(const std::vector<std::vector<TileInfo>>* tileInfo, const LevelPoint start, const LevelPoint target);	// calculate shortest path between start and target
    LevelPoint FindFarthestTile(const std::vector<std::vector<TileInfo>>* tileInfo, const LevelPoint& start);	// find the tile that is farthest from start (and has a valid path)
    
private:
	std::vector<std::vector<int>> ComputeDistances(const std::vector<std::vector<TileInfo>>* tileInfo, const LevelPoint& start);	// compute distances for all tiles from start
	std::vector<LevelPoint> GetNeighbors(const std::vector<std::vector<TileInfo>>* tileInfo, const LevelPoint& curr); // get neighbor tiles that allow movement
};
