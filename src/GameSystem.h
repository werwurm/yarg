#pragma once

#include "Player.h"
#include "Interface.h"
#include "Level.h"
#include "Frame.h"
#include "MapGenerator.h"
#include "GameClock.h"
#include "Visibility.h"


class GameSystem
{
public:
    GameSystem(const std::vector<EnemyData>* enemiesConfig, const std::vector<MapData>* mapTilesConfig);
	~GameSystem();
	
    Interface interface;

    void playGame();
    bool playerMove();
	void endGame();

private:
	std::shared_ptr<GameClock> m_clock; 
    std::vector<std::unique_ptr<Level>> m_levelList;
    int m_levelIdx = 0;
    
    MapGenerator m_mapGenerator;
    Player* m_player = new Player('@', "the player" , 1,100,10,10, 5, {"the bold player"}, {"YARG!!!"}, 0 , 100);	// TODO: HARDCODED SPEED!"
    Frame m_map;
    Frame m_interface;
    Frame m_gamelog;
    
   	void generateProceduralMap(uint seed, std::unique_ptr<Level>& level, const std::vector<EnemyData>* enemiesConfig, const std::vector<MapData>* mapTilesConfig);
   	void generateLevel(const std::vector<EnemyData>* enemiesConfig, const std::vector<MapData>* mapTilesConfig);
   	bool checkForLevelSwitch(LevelPoint &playerPos, RayCastVisibility& playerVisibility, RayCastVisibility& enemyVisibility, bool &firstMove);
   	void switchLevel(const uint lvlNr, RayCastVisibility& playerVisibility, RayCastVisibility& enemyVisibility, bool levelDown);
};

