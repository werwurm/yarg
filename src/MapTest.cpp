#include <iostream>
#include <random>
#include <ctime>
#include "MapGenerator.h"
#include "ConfigParser.h"
#include "Datatypes.h"

void printMap(const std::vector<std::string>& map, const std::vector<std::string>& enemyLayer)
{
    for (uint i = 0; i< map.size(); i++)
    {
        for (uint j=0; j<map[0].size(); j++)
        {
			if(enemyLayer[i][j] != ' ') {
				std::cout << enemyLayer[i][j];
			}
			else {
				std::cout << map[i][j];
			}
        }
        std::cout << std::endl;
    }
}

GameConfig gameConfig;	// make this config global

int main()
{ 

	std::srand(std::time(nullptr));  // seed the random number generator with current time
	uint seed = std::rand();

	gameConfig = cfg_readGameConfig("settings.cfg");
  	cfg_printConfigData(gameConfig);
  	
  	if(gameConfig.seed == 0) {
		gameConfig.seed = seed;
	}

	std::vector<MapData> mapData = cfg_readMapConfig(gameConfig.MapConfigFile);
	std::vector<EnemyData> enemiesData = cfg_readEnemiesConfig(gameConfig.EnemyConfigFile);

    MapGenerator generator;
    auto map = generator.CellularAutomata(gameConfig.seed,
								gameConfig.mapWidth, 
								gameConfig.mapHeight, 
								gameConfig.CellAutIterations, 
								gameConfig.CellAutPercentWalls,  
								&mapData);
	
	//auto map = generator.DrunkardsWalk(gameConfig.seed,
								//gameConfig.mapWidth, 
								//gameConfig.mapHeight, 
								//gameConfig.DrunkardPathLength,
								//gameConfig.DrunkardPercentWalls,
								//gameConfig.DrunkardStartCenter, 
								//WALL_TILE, 
								//FLOOR_TILE);
								
    generator.connectIsolatedAreas(map, &mapData);
    generator.PlaceObjects(map, seed, &mapData);
    
    generator.AddLakes(map, gameConfig.seed, gameConfig.nrLakes, gameConfig.minLakeSize, gameConfig.maxLakeSize, &mapData);
    generator.AddMoss(map, gameConfig.seed, gameConfig.mossNextWallProb, gameConfig.mossNextWaterProb, gameConfig.mossNextWaterWallProb, &mapData);
    generator.AddTrees(map, gameConfig.seed, gameConfig.treeProb, gameConfig.treeNeighbourProb, &mapData);
    generator.AddGates(map, gameConfig.seed, gameConfig.gateWallProb, gameConfig.gateWaterProb, &mapData);
    generator.FixBorders(map, &mapData);

    
    //// add enemies
    auto enemyLayer = generator.PlaceEnemies(map, gameConfig.seed, 
								&mapData);

    printMap(map, enemyLayer);

	std::cout << "Your Seed was: " << gameConfig.seed << std::endl;
    return 0;
}


