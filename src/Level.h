#pragma once
#include <vector>
#include <string>
#include <memory>

#include "Datatypes.h"
#include "Player.h"
#include "Enemy.h"
#include "Frame.h"
#include "Interface.h"
#include "ConfigParser.h"
#include "GameClock.h"

class Level
{
public:
    Level(std::shared_ptr<GameClock> clock);
    ~Level();

    void load(std::string fileName, Player* player);
    void loadLevel(std::string fileName);	// Load Level data from text file to _levelData
    void processLevel(Player* player, const std::vector<EnemyData>* enemiesConfig, const std::vector<MapData>* tilesDataPtr);	// set enemy and players position, set map layers
    void setUpMap(std::vector<std::string> &map, const std::vector<MapData>* tilesDataPtr); 	// set up map data structures
	void setUpEnemies(Player* player, const std::vector<EnemyData>* enemiesConfig, const std::vector<std::string>& enemyMap);  // set up enemy data structures
    void setInterface(Interface *theInterface);
    void updateMap(Frame &map, std::vector<std::vector<FoVInfo>>* fovInfo);
    void print() const;

    void movePlayer(const char input, Player* player, Interface &theInterface);    // Evaluate commands to move character
    void updateEnemies(Player* player);
    
    // Getters
    char getTile(const LevelPoint pos) const; 							// get object on Tile x,y
    char getEnemyTile(const LevelPoint pos) const; 						// get object on Tile x,y from enemy map layer 
    bool getTranslucent(const LevelPoint pos) const; 						// get information if tile blocks light
    bool getBlocksLight(const LevelPoint pos) const;						// opposite getter of getTranslucent (true/1 if blocks light, false/0 if translucent)
    MapSize getMapSize() const;
    std::shared_ptr<Enemy>* getEnemy(unsigned int ListPos);			// get Enemy handle for ListPos in _enemies 
    uint getNrEnemies();											// get Nr. of enemies currently in _enemies
    const std::vector<std::vector<TileInfo>>* getTileInfo() const;	// get handle for _tileInfo
	const std::deque<TimedEntity*>& getTimedEntitiesPtr() const; 	// Getter function for m_timedEntities
	const LevelPoint getStartPos();									// Getter for player starting position in this Level
	std::vector<std::vector<FoVInfo>>* getFoVPtr();					// get handle for FoV matrix
	LevelPoint getLadderUpPosition();								// getter for Level start
	LevelPoint getLadderDownPosition();								// getter for Level end
	
    // Setters
    void setTile(LevelPoint pos, char tile);  // set Tile x,y to Object
    void setPlayerFoVInfo(std::vector<std::vector<FoVInfo>> &playerFoVInfo);

private:
	std::shared_ptr<GameClock> m_clock;
    MapSize m_mapSize;
    std::deque<TimedEntity*> m_timedEntities;		// a list of entities that use the concept of time

	// different map layers
    std::vector <std::string> m_levelData;     // Holds the level structure, each vector row contains a line of the level
    std::vector<std::vector<TileInfo>> m_tileInfo; // Holds additional data for each tile
    std::vector <std::string> m_enemyLayer;	// extra map layer that contains enemies
    
    std::vector<std::shared_ptr<Enemy>> m_enemies;	// vector with all enemies within level
    const std::vector<EnemyData>* m_enemiesConfig;	// pointer to vector with enemy configuration (all monsters are user defined at runtime)
    const std::vector<MapData>* m_mapConfig;		// pointer to vector with map tile configuration 
   
    Interface *m_theInterface;    // pointer to the game interface
    
    LevelPoint m_ladderUpPosition;
    LevelPoint m_ladderDownPosition;
    
    std::vector<std::vector<FoVInfo>> m_playerFoVInfo; 	// stores which parts of the map were discovered when switching levels
    
    // private methods
    void processPlayerMove(Player* player, LevelPoint targetPos);   // check if player can move to new tile and execute move, if so
    void addActionPointBonuses(const Player* player, const uint movementCosts) const;		// if player has higher movement costs, give AP bonus to all other entities
    
    void processEnemyMove(Player* player, std::shared_ptr<Enemy> enemy, LevelPoint targetPos); // check if enemy can move to new tile and execute move, returns true if monster is still alive, false otherwise   
    void handleKilledEnemy(std::shared_ptr<Enemy> enemy, LevelPoint enemyPos, Player* player);		// remove killed enemies from map and m_timedEntities
    void battleEnemy(Player* player, LevelPoint targetPos);   // battle Monster at this position, returns true if monster is still alive, false if it got killed
};
