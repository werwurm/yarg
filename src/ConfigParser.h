#pragma once

#include <string>
#include <vector>

// structs that will hold config data

// represents data neccessary to call the enemy() constructor
struct EnemyData {
    char tile;
    std::string name;
    uint level;
    int health;
    uint attack;
    uint defense;
    uint experienceValue;
    uint visibilityRange;
    int speed;
    std::vector<std::string> descriptions;
    std::vector<std::string> battleCries;
};

// represents map tiles
struct MapData {
    char tile = '?';
    bool blocks = false;         
    bool translucent = true;   
    std::string color;
    uint movementCost = 100;
    std::string name;
    std::string description;
    
    float attackModifier = 0;
    float defenceModifier = 0;
};
     
// represents game config
struct GameConfig {
	std::string MapConfigFile;	// config file with definition of each map tile
	std::string EnemyConfigFile;	// config file with definition of enemies
	
	// level config
	uint nrLevels = 0;
	std::vector<std::string> levelNames;
	
	// cheat modes
	bool mindVision;	// player sees enemies including their FoV
	bool playerFoV;		// limited vs. unlimited FoV
	
	// Map Generator
	uint seed;
	uint mapWidth = 80;
	uint mapHeight = 35;
	
	uint CellAutIterations = 4;
	uint CellAutPercentWalls = 45;
	
	uint DrunkardPathLength = 100;       // how many cells should each Drunkard randomly walk, default 100
    uint DrunkardPercentWalls = 45;      // percentage of walls for final map, default 45
    bool DrunkardStartCenter = true;     // true, default: each path starts in the center of the map, false: each path starts randomly
	
	uint nrLakes = 15;
	uint minLakeSize = 8;
	uint maxLakeSize = 50;
	
	uint mossNextWaterProb = 60;	// prob. of moss spawning next to water
	uint mossNextWallProb = 25;    // prob. of moss spawning next to 2 or more adjacent walls
	uint mossNextWaterWallProb = 80; // prob of moss spawning next to water AND adjacent walls
	
	uint treeProb = 50;           // probability for a tree to grow withing a free 3x3 area, default 50
    uint treeNeighbourProb = 40;  // probability for a tree to have neighbour trees, default 40#
    
    uint gateWallProb = 30;       // probability for a wall tile (if surrounded by floor or gate tiles) to be replaced by gate
    uint gateWaterProb = 90;      // probability for a wall tile adjacent to water to be replaced by gate
};
   
std::vector<EnemyData> cfg_readEnemiesConfig(const std::string& configFile); // Read enemy configuration file and store data in a vector
int cfg_EnemiesDataPos(char tile, const std::vector<EnemyData>* enemies); // returns position of given enemy in parsed enemy list, -1 if not in list
void cfg_printEnemiesData(const std::vector<EnemyData>* enemiesDataPtr); // Debug routine, printout parsed enemy config file

std::vector<MapData> cfg_readMapConfig(const std::string& configFile);		// read and parse map config, store in vector
int cfg_MapTileDataPos(char tile, const std::vector<MapData>* tilesDataPtr); // checks if a given tile was configured and is in MapData vector
char cfg_getMapTileByName(const std::vector<MapData>& mapData, const std::string& name);  // returns map tile character that belongs to a map tile name

void cfg_printMapData(const std::vector<MapData>* tilesDataPtr); // Debug routine, printout parsed map config file

GameConfig cfg_readGameConfig(const std::string& configFile); // read game config and store data in struct
void cfg_printConfigData(const GameConfig& configPtr);	// debug routine, printout parsed config
