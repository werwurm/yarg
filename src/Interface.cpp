#include <cstring>
#include <memory>

#include "Interface.h"
#include "Datatypes.h"

Interface::Interface(){
}

void Interface::init(MapSize mapSize){

    map.init(mapSize.Height, mapSize.Width, 0, 0);         // frame for the whole map
    mapWin.init(map, 18, 60, 0, 0);             // frame for the map viewport in interface
    commandWin.init(3, 50, 17 + 1, 1);    // interface directly below the map
    gameLog.init(8, 50, 22, 1);       // all game messages go here
    playerStatus.init(8, 20, 0, 60 + 2);  // status Window for player
    enemyStatus.init(8, 20, 10, 60 + 2);  // status window for enemy currently being battled
    
    #ifdef DEBUG_CONSOLE
        m_debugMsgIdx = 0;
        debugWin.init(18, 50, 0, 82);
    #endif

    m_messageIndex = 0;      // nr of messages in gameLog (0.. MAX_MESSAGES)

    box(gameLog.getWin(), 0, 0);     // draw box around gameLog window
    box(playerStatus.getWin(), 0, 0);
    box(enemyStatus.getWin(), 0, 0);
    
    #ifdef DEBUG_CONSOLE
        box(debugWin.getWin(),0,0);
    #endif
}

void Interface::refreshAll(){
    mapWin.refresh();
    commandWin.refresh();
    gameLog.refresh();
    playerStatus.refresh();
    enemyStatus.refresh();
    
    #ifdef DEBUG_CONSOLE
        debugWin.refresh();
    #endif
}

void Interface::refreshComponent(Frame &interfaceComp){
    interfaceComp.refresh();
}

//void Interface::addMsg(Frame &targetFrame, char const *text){
    //waddstr(targetFrame.getWin(), text);
//}
void Interface::addMsg(Frame &targetFrame, const char* format, ...) {
    va_list args;
    va_start(args, format);
    vwprintw(targetFrame.getWin(), format, args);
    va_end(args);
}


// add a message line to a scrolling game log window
// variadic input like printf(), accepts more than one parameter, example:
// _theInterface->addGameLog("%s attacks Player with a roll of %d  ", enemyName.c_str(), attackRoll);
// written by ChatGPT (no kidding)
void Interface::addGameLog(char const *fmt, ...){
    va_list args;
    va_start(args, fmt);
    char text[MESSAGE_LENGTH];
    vsnprintf(text, MESSAGE_LENGTH, fmt, args);
    va_end(args);

    // Allocate memory for the message string and copy the text into it
    char *message = new char[MESSAGE_LENGTH + 1];
    strncpy(message, text, MESSAGE_LENGTH);
    message[MESSAGE_LENGTH] = '\0';

    // Check if we have exceeded the maximum number of messages
    if (m_messageIndex < MAX_MESSAGES) {
        // nope, simply add the message to the array
        m_gameLogMsgs[m_messageIndex] = message;
        m_messageIndex++;
    } else {
        // yep, limit exceeded. we need to remove the oldest message from the array
        // and shift all the messages by one to make space for the new message
        delete[] m_gameLogMsgs[0];
        for (int i = 0; i < MAX_MESSAGES - 1; i++) {
            m_gameLogMsgs[i] = m_gameLogMsgs[i + 1];
        }
        m_gameLogMsgs[MAX_MESSAGES - 1] = message;
    }

    // Clear the gameLog window and add all the messages in the array to it
    WINDOW *gamelogWin = gameLog.getWin();
    wclear(gamelogWin);
    for (int i = 0; i < m_messageIndex; i++) {
        wmove(gamelogWin, i+1, 1); // move to second column
        waddstr(gamelogWin, m_gameLogMsgs[i]);
        waddch(gamelogWin, '\n');
    }

    box(gamelogWin, 0, 0); // Redraw box around gameLog window
    wrefresh(gamelogWin); // Refresh gameLog window
}

void Interface::updatePlayerStatus(int level, int health, int attack, int defence, int experience){
    WINDOW *statusWin = playerStatus.getWin();
    wclear(statusWin);
    wmove(statusWin,1,2); wprintw(statusWin,"Player Stats\n");
    wmove(statusWin,2,3); wprintw(statusWin,"LVL:  %d\n", level);
    wmove(statusWin,3,3);wprintw(statusWin, "HLTH: %d\n", health);
    wmove(statusWin,4,3);wprintw(statusWin, "ATT:  %d\n", attack);
    wmove(statusWin,5,3);wprintw(statusWin, "DEF:  %d\n", defence);
    wmove(statusWin,6,3);wprintw(statusWin, "EXP:  %d\n", experience);

    box(statusWin, 0, 0);
    wrefresh(statusWin);
}

void Interface::updateEnemyStatus(char const *name, int level, int attack, int defense, int health){
    WINDOW *enemyWin = enemyStatus.getWin();
    wclear(enemyWin);
    wmove(enemyWin,1,2); wprintw(enemyWin, name);
    wmove(enemyWin,2,3); wprintw(enemyWin, "LVL:  %d\n", level);
    wmove(enemyWin,3,3); wprintw(enemyWin, "HLTH: %d\n", health);
    wmove(enemyWin,4,3); wprintw(enemyWin, "ATT:  %d\n", attack);
    wmove(enemyWin,5,3); wprintw(enemyWin, "DEF:  %d\n", defense);

    box(enemyWin, 0, 0);
    wrefresh(enemyWin);
}

#ifdef DEBUG_CONSOLE
    // add a message line to a scrolling debug log window
    // variadic input like printf(), accepts more than one parameter, example:

    void Interface::addDebugMsg(char const *fmt, ...){
        va_list args;
        va_start(args, fmt);
        char text[DEBUG_MSG_LENGTH];
        vsnprintf(text, DEBUG_MSG_LENGTH, fmt, args);
        va_end(args);

        // Allocate memory for the message string and copy the text into it
        char *message = new char[DEBUG_MSG_LENGTH + 1];
        strncpy(message, text, DEBUG_MSG_LENGTH);
        message[DEBUG_MSG_LENGTH] = '\0';

        // Check if we have exceeded the maximum number of messages
        if (m_debugMsgIdx < MAX_DEBUG_MSG) {
            // nope, simply add the message to the array
            m_debugMsgs[m_debugMsgIdx] = message;
            m_debugMsgIdx++;
        } else {
            // yep, limit exceeded. we need to remove the oldest message from the array
            // and shift all the messages by one to make space for the new message
            delete[] m_debugMsgs[0];
            for (int i = 0; i < MAX_DEBUG_MSG - 1; i++) {
                m_debugMsgs[i] = m_debugMsgs[i + 1];
            }
            m_debugMsgs[MAX_DEBUG_MSG - 1] = message;
        }

        // Clear the gameLog window and add all the messages in the array to it
        WINDOW *dbgWin = debugWin.getWin();
        wclear(dbgWin);
        for (int i = 0; i < m_debugMsgIdx; i++) {
            wmove(dbgWin, i+1, 1); // move to second column
            waddstr(dbgWin, m_debugMsgs[i]);
            waddch(dbgWin, '\n');
        }

        box(dbgWin, 0, 0); // Redraw box around gameLog window
        wrefresh(dbgWin); // Refresh gameLog window
    }
#endif



void Interface::clearWin(Frame &targetFrame){
     wclear(targetFrame.getWin());
}

void Interface::clearAll(){
    wclear(map.getWin());
    wclear(commandWin.getWin());
    wclear(gameLog.getWin());
}

