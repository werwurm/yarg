#pragma once
#include <ncurses.h>

class Frame {
public:
    Frame();
	void init(int nr_rows, int nr_cols, int row_0, int col_0); 	// Initialize a main window (no parent)
	void init(Frame &sw, int nr_rows, int nr_cols, int row_0, int col_0);  	// Initialize a subwindow (viewport) with a parent window
	~Frame();

    // Getters
    WINDOW *getWin() const; 	       // Get the window
	WINDOW *getParentWin() const;    // Get the parent window
	bool hasParent() const;          // Get window type, if TRUE we have a subwindow, if FALSE we have a main window
	int getHeight() const;           // Get the height of the window
	int getWidth() const;        	   // Get the width of the window
	int row() const;                 // Get the row (y) position of the window
	int col() const;                 // Get the row (y) position of the window

	void refresh() const;            	// Refresh the window
	void move(int r, int c);   	// Move a window in a new position (r, c)
	void center(int playerX, int playerY); 	// Center the viewport around a character

private:
	int m_height, m_width;   // Frame dimensions
	int m_row, m_col;        // Frame position

	bool m_hasParent;      // Boolean - FALSE for a window and TRUE for a subwindow (viewport)
	WINDOW *m_window;       // Pointer to an ncurses WINDOW
	WINDOW *m_parentWindow; // Pointer to an ncurses WINDOW, this will be NULL for a window and will point to the parent window
                           // for a subwindow
};

