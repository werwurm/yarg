#include "Screen.h"

// Initialize ncurses library
Screen::Screen(){
    initscr();
    clear();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    curs_set(0);

    // get screen dimensions
    getmaxyx(stdscr, m_height, m_width);
}

// clear ncurses
Screen::~Screen(){
    endwin();
}

// Print a message on the screen
void Screen::add(const char *message) {
	printw(message);
}

// Get the screen height
int Screen::getHeight() const{
	return m_height;
}

// Get the screen width
int Screen::getWidth() const{
	return m_width;
}
