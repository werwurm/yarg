#include <fstream>
#include <iostream>
#include <limits>
#include <random>
#include <ncurses.h>
#include <memory>
#include <type_traits>
#include <algorithm>

#include "Level.h"
#include "Datatypes.h"
#include "ConfigParser.h"
#include "Visibility.h"

extern GameConfig gameConfig;
    
Level::Level(std::shared_ptr<GameClock> clock) : m_clock(clock),
							m_tileInfo(), m_enemyLayer(), m_theInterface(nullptr){
   
	m_mapSize = {gameConfig.mapWidth, gameConfig.mapHeight};
	
	// resize additional map layers to map size
	m_tileInfo.resize(m_mapSize.Height);
	m_enemyLayer.resize(m_mapSize.Height); 
    
	for (uint i = 0; i < m_mapSize.Height; i++) {  // Loop through each row of the Level string
		m_tileInfo[i].resize(m_mapSize.Width); // for the current map row also add a row to the m_tileInfo data structure
		m_enemyLayer[i].resize(m_mapSize.Width); 	// and the enemy layer, too
		fill(m_tileInfo[i].begin(), m_tileInfo[i].end(), TileInfo{false, true, 100, "name placeholder", "description placeholder"});   // init this line with defaults
		fill(m_enemyLayer[i].begin(), m_enemyLayer[i].end(), ' ');	// no monsters yet
		}

	clock->registerWithTimekeeper(m_timedEntities);
	
	// prepare FoV info for level change
    m_playerFoVInfo.resize(m_mapSize.Height);	// extend vector to height components
    for(uint i=0; i<m_mapSize.Height; i++) {
		m_playerFoVInfo[i].resize(m_mapSize.Width);		// each height row needs a line of width length
		fill(m_playerFoVInfo[i].begin(), m_playerFoVInfo[i].end(), FoVInfo{false, false});	// init each line
	}
	
	RayCastVisibility vis(this);
	vis.SetAllInvisible(&m_playerFoVInfo);
	vis.SetAllUndiscovered(&m_playerFoVInfo);
}

Level::~Level() {
	//delete clock;
}

// Load Level data from text file to m_levelData
// note that _leveData will contain only the ASCII layout of the map- it
// has no idea about the attributes (like does this tile block movement,
// is translucent, ...). The parsing of the map is done in processLevel()
void Level::loadLevel(std::string fileName){

    // Load the Level
    std::ifstream file;

    file.open(fileName);
    if(file.fail()) {
        perror(fileName.c_str());
        std::cout << "Press any key to continue . . .";
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        exit(1);
    }

    std::string line;

    while(std::getline(file, line)) {
        m_levelData.push_back(line);
    }

    file.close();
    
    //get map size
    uint maxX = 0;  // maximum columns on map
    uint nrRows = m_levelData.size();
    
    for (uint i = 0; i< nrRows; i++) {
        if (m_levelData[i].length() > maxX) {
            maxX = m_levelData[i].length();
        }
    }
    
    this->m_mapSize = {maxX , nrRows};
}


// processLevel() is called after LoadLevel() to parse the information stored
// in the m_levelData vector<string> and set up the neccessary data structures 
// for the game engine
void Level::processLevel(Player* player, 
					const std::vector<EnemyData>* enemiesConfig, 
					const std::vector<MapData>* tilesDataPtr)
{
    char tile = '!';  // symbol for current tile
	
	// resize additional map layers to level size
    m_tileInfo.resize(m_mapSize.Height);
    m_enemyLayer.resize(m_mapSize.Height); 
    
    for (uint i = 0; i < m_mapSize.Height; i++) {  // Loop through each row of the Level string

        m_tileInfo[i].resize(m_mapSize.Width); // for the current map row also add a row to the m_tileInfo data structure
        m_enemyLayer[i].resize(m_mapSize.Width); 	// and the enemy layer, too
        fill(m_tileInfo[i].begin(), m_tileInfo[i].end(), TileInfo{false, true, 100, "name placeholder", "description placeholder"});   // init this line with defaults
		fill(m_enemyLayer[i].begin(), m_enemyLayer[i].end(), ' ');	// no monsters yet

        for(uint j = 0; j < m_mapSize.Width; j++) {  // Loop through each letter in that row

            tile = m_levelData[i][j];
            
            // the following takes care of all tiles that represent an enemy
            // as defined in ressources/Monsters.cfg
			int enemyListPos = cfg_EnemiesDataPos(tile, enemiesConfig);	// check if current tile is represented in the parsed config file
			if(enemyListPos != -1) {	// -1 means the tile is not in enemy config tile
				std::shared_ptr<Enemy> tmpEn = std::make_shared<Enemy>(
								enemiesConfig->at(enemyListPos).tile,
								enemiesConfig->at(enemyListPos).name,
								enemiesConfig->at(enemyListPos).level,
								enemiesConfig->at(enemyListPos).health,
								enemiesConfig->at(enemyListPos).attack,
								enemiesConfig->at(enemyListPos).defense,
								enemiesConfig->at(enemyListPos).visibilityRange,
								enemiesConfig->at(enemyListPos).descriptions,
								enemiesConfig->at(enemyListPos).battleCries,
								enemiesConfig->at(enemyListPos).experienceValue,
								enemiesConfig->at(enemyListPos).speed);
				m_enemies.push_back(tmpEn);	
				m_enemyLayer[i][j] = enemiesConfig->at(enemyListPos).tile;				// position enemy on enemy map layer 
				m_enemies.back()->setPosition((LevelPoint){j,i});		// set its position in the enemy object  
				m_enemies.back()->registerWithTimekeeper(m_timedEntities);	// register in the queue of all timed entities			
                m_levelData[i][j] = '.';					// m_levelData will now hold only static data
				}
			// the following deals with the fixed map tiles and the attributes
			// defined in the map config file
			int tileCfgPos = cfg_MapTileDataPos(tile, tilesDataPtr);
			
			if(tileCfgPos != -1) { 		// -1 means the tile is not defined in map config file
				m_tileInfo[i][j].blocks = tilesDataPtr->at(tileCfgPos).blocks;	
				m_tileInfo[i][j].translucent = tilesDataPtr->at(tileCfgPos).translucent;
				m_tileInfo[i][j].movementCost = tilesDataPtr->at(tileCfgPos).movementCost;
				m_tileInfo[i][j].name = tilesDataPtr->at(tileCfgPos).name;
				m_tileInfo[i][j].description = tilesDataPtr->at(tileCfgPos).description;
				
				m_tileInfo[i][j].battleModifiers.attack = tilesDataPtr->at(tileCfgPos).attackModifier;
				m_tileInfo[i][j].battleModifiers.defence = tilesDataPtr->at(tileCfgPos).defenceModifier;
			}		// more attributes to come!
			
			if(m_tileInfo[i][j].name == "stairsUp") {	// store the coordinates of the upwards stairs for player start position
				m_ladderUpPosition.Y = i;
				m_ladderUpPosition.X = j;
				player->setPosition((LevelPoint){j,i});
				m_enemyLayer[i][j] = '@';
				player->registerWithTimekeeper(m_timedEntities);	// register player as a timed entity
				}
			else if(m_tileInfo[i][j].name == "stairsDown") {
				m_ladderDownPosition.Y = i;
				m_ladderDownPosition.X = j;
				}
        }
    }
    
    // prepare FoV fields for player
    player->m_fovInfo.resize(m_mapSize.Height);	// extend vector to height components
    for(uint i=0; i<m_mapSize.Height; i++) {
		player->m_fovInfo[i].resize(m_mapSize.Width);		// each height row needs a line of width length
		fill(player->m_fovInfo[i].begin(), player->m_fovInfo[i].end(), FoVInfo{false, false});	// init each line
	}
	
	// prepare Fov for all enemies
    for(uint i=0; i < m_enemies.size(); i++){
		m_enemies[i]->m_fovInfo.resize(m_mapSize.Height);
		for(uint j=0; j<m_mapSize.Height; j++) {
			m_enemies[i]->m_fovInfo[j].resize(m_mapSize.Width);
			fill(m_enemies[i]->m_fovInfo[j].begin(), m_enemies[i]->m_fovInfo[j].end(), FoVInfo{false, false});
		}
	}
	
	// store pointer to enemy and map configuration for later access
	m_enemiesConfig = enemiesConfig;
	m_mapConfig = tilesDataPtr;
}

// this is a lot of duplicated code from processLevel()
// method sets up the map tile structures, takes the map layer generated by MapGenerator as input
// TODO: find a more elegant way to have both loading text files as map and procedurally generating them
void Level::setUpMap(std::vector<std::string> &map, const std::vector<MapData>* tilesDataPtr){
	    char tile = '!';  // symbol for current tile
	
	// resize additional map layers to level size
    m_tileInfo.resize(m_mapSize.Height);
    m_enemyLayer.resize(m_mapSize.Height); 
    
    for (uint i = 0; i < m_mapSize.Height; i++) {  // Loop through each row of the Level string

        m_tileInfo[i].resize(m_mapSize.Width); // for the current map row also add a row to the m_tileInfo data structure
        fill(m_tileInfo[i].begin(), m_tileInfo[i].end(), TileInfo{false, true, 100, "name placeholder", "description placeholder"});   // init this line with defaults

        for(uint j = 0; j < m_mapSize.Width; j++) {  // Loop through each letter in that row

			m_levelData = map;
            tile = m_levelData[i][j];
            
			// the following deals with the fixed map tiles and the attributes
			// defined in the map config file
			int tileCfgPos = cfg_MapTileDataPos(tile, tilesDataPtr);
			
			if(tileCfgPos != -1) { 		// -1 means the tile is not defined in map config file
				m_tileInfo[i][j].blocks = tilesDataPtr->at(tileCfgPos).blocks;	
				m_tileInfo[i][j].translucent = tilesDataPtr->at(tileCfgPos).translucent;
				m_tileInfo[i][j].movementCost = tilesDataPtr->at(tileCfgPos).movementCost;
				m_tileInfo[i][j].name = tilesDataPtr->at(tileCfgPos).name;
				m_tileInfo[i][j].description = tilesDataPtr->at(tileCfgPos).description;
				
				m_tileInfo[i][j].battleModifiers.attack = tilesDataPtr->at(tileCfgPos).attackModifier;
				m_tileInfo[i][j].battleModifiers.defence = tilesDataPtr->at(tileCfgPos).defenceModifier;
				
				if(m_tileInfo[i][j].name == "stairsUp") {	// store the coordinates of the upwards stairs for player start position
					m_ladderUpPosition.Y = i;
					m_ladderUpPosition.X = j;
				}
				else if(m_tileInfo[i][j].name == "stairsDown") {
					m_ladderDownPosition.Y = i;
					m_ladderDownPosition.X = j;
				}
			}		// more attributes to come!
        }
    }
    	
	// store pointer to map configuration for later access
	m_mapConfig = tilesDataPtr;
}

// this is a lot of duplicated code from processLevel()
// method sets up the enemies structures, takes the enemy layer generated by MapGenerator as input
// TODO: find a more elegant way to have both loading text files as map and procedurally generating them
void Level::setUpEnemies(Player* player, const std::vector<EnemyData>* enemiesConfig, const std::vector<std::string>& enemyMap){
	char tile = '!';  // symbol for current tile
    
    for (uint i = 0; i < m_mapSize.Height; i++) {  // Loop through each row of the Level string

		fill(m_enemyLayer[i].begin(), m_enemyLayer[i].end(), ' ');	// no monsters yet

        for(uint j = 0; j < m_mapSize.Width; j++) {  // Loop through each letter in that row

            tile = enemyMap[i][j];
            
            // the following takes care of all tiles that represent an enemy
            // as defined in ressources/Monsters.cfg
			int enemyListPos = cfg_EnemiesDataPos(tile, enemiesConfig);	// check if current tile is represented in the parsed config file
			if(enemyListPos != -1) {	// -1 means the tile is not in enemy config tile
				std::shared_ptr<Enemy> tmpEn = std::make_shared<Enemy>(		// create temporary enemy object
								enemiesConfig->at(enemyListPos).tile,
								enemiesConfig->at(enemyListPos).name,
								enemiesConfig->at(enemyListPos).level,
								enemiesConfig->at(enemyListPos).health,
								enemiesConfig->at(enemyListPos).attack,
								enemiesConfig->at(enemyListPos).defense,
								enemiesConfig->at(enemyListPos).visibilityRange,
								enemiesConfig->at(enemyListPos).descriptions,
								enemiesConfig->at(enemyListPos).battleCries,
								enemiesConfig->at(enemyListPos).experienceValue,
								enemiesConfig->at(enemyListPos).speed);
				m_enemies.push_back(tmpEn);		// push it to enemy list
				m_enemyLayer[i][j] = enemiesConfig->at(enemyListPos).tile;				// position enemy on enemy map layer 
				m_enemies.back()->setPosition((LevelPoint){j,i});		// set its position in the enemy object
				m_enemies.back()->registerWithTimekeeper(m_timedEntities);	// register in the queue of all timed entities
				}
            switch (tile) { // TODO: hardcoded symbols!
                case '@':      // the player character
                    player->setPosition((LevelPoint){j,i});
                    m_enemyLayer[i][j] = '@';
                    player->registerWithTimekeeper(m_timedEntities);	// register player as timed entity
                    break;
                default:
                    break;
            }
        }
    }
    
    // prepare and init FoV fields for player
    player->m_fovInfo.resize(m_mapSize.Height);	// extend vector to height components
    for(uint i=0; i<m_mapSize.Height; i++) {
		player->m_fovInfo[i].resize(m_mapSize.Width);		// each height row needs a line of width length
		fill(player->m_fovInfo[i].begin(), player->m_fovInfo[i].end(), FoVInfo{false, false});	// init each line
	}
	
	// prepare and init Fov for all enemies
    for(uint i=0; i < m_enemies.size(); i++){
		m_enemies[i]->m_fovInfo.resize(m_mapSize.Height);
		for(uint j=0; j<m_mapSize.Height; j++) {
			m_enemies[i]->m_fovInfo[j].resize(m_mapSize.Width);
			fill(m_enemies[i]->m_fovInfo[j].begin(), m_enemies[i]->m_fovInfo[j].end(), FoVInfo{false, false});
		}
	}
		
	// store pointer to enemy configuration for later access
	m_enemiesConfig = enemiesConfig;
}

void Level::setInterface(Interface *theInterface){
    m_theInterface = theInterface;
}

// copy map content to ncurses window
// this is the actual render loop
void Level::updateMap(Frame &map, std::vector<std::vector<FoVInfo>>* fovInfo){
    
    WINDOW *mapWin = map.getWin();  // get map handle
	
	start_color();
    init_pair(1, COLOR_WHITE, COLOR_BLACK);		// visible map: white on black background
    init_pair(2, COLOR_BLACK, COLOR_BLACK); // discovered but currently not visible map: grey on black background
											// (we intensify 'COLOR_BLACK' to grey by using the BOLD attribute		
	init_pair(4, COLOR_YELLOW, COLOR_BLACK);	// can be switched to brown using A_DIM attr
	init_pair(5, COLOR_GREEN, COLOR_BLACK);
	init_pair(6, COLOR_BLUE, COLOR_BLACK);
	init_pair(7, COLOR_RED, COLOR_BLACK);
	init_pair(8, COLOR_MAGENTA, COLOR_BLACK);
	init_pair(9, COLOR_CYAN, COLOR_BLACK);
	
	// create a new vector of vector of vector of FoVInfo objects
	//vector<vector<vector<FoVInfo>>>* enemyFoV = new vector<vector<vector<FoVInfo>>>(m_enemies.size());
	
	// =================================================================
	// TODO: remove following line, for debugging enemy fov only
	//if(gameConfig.mindVision) {
		//// loop through all enemies and assign their FoVInfo to the enemyFoV vector
		//for (uint i = 0; i < m_enemies.size(); i++) {
			//(*enemyFoV)[i] = *(m_enemies[i].getFoVInfo());
		//}
		//init_pair(3, COLOR_RED, COLOR_BLACK);	// enemy FoV
	//}
	//vector<vector<FoVInfo>>*enemyFov = m_enemies[0].getFoVInfo();
	//init_pair(3, COLOR_RED, COLOR_BLACK);	// enemy FoV
	// ============== End of debug enemy FoV ===========================
	
										
	wattron(mapWin, A_BOLD);				
	
	// special code for handling mind vision
	if(gameConfig.mindVision)
	{
		for(uint k=0; k< m_enemies.size(); k++)
		{
			LevelPoint enemyPos; 
			m_enemies[k]->getPosition(enemyPos);
			(*fovInfo)[enemyPos.Y][enemyPos.X].visible = true;
			(*fovInfo)[enemyPos.Y][enemyPos.X].discovered = true;
			
		}
	}
	
							
    for(unsigned int i = 0; i < m_mapSize.Height; i++){
        for(unsigned int j=0; j< m_mapSize.Width; j++){      // loop through map row 		              
            if((*fovInfo)[i][j].visible) {		// is tile currently visible?
				wattron(mapWin, COLOR_PAIR(1));				// yes, draw in white on black
				
				if(m_enemyLayer[i][j] != ' ') {				// is current tile occupied by enemy?
					waddch(mapWin, m_enemyLayer[i][j]);		// then draw its tile
				}
				else {
					char tile = m_levelData[i][j];
					int TileIdx = cfg_MapTileDataPos(tile, m_mapConfig);
					if(TileIdx != -1) {
						std::string tileCol = m_mapConfig->at(TileIdx).color;
						// What an ugly clusterfuck. this alone is worth 
						// considering the switch to graphical output and 
						// better rendering capabilies
						if( tileCol == "BROWN"){
							wattron(mapWin, COLOR_PAIR(4));
							wattron(mapWin, A_DIM);
							waddch(mapWin, m_levelData[i][j]);
							wattroff(mapWin, COLOR_PAIR(4));
							wattroff(mapWin, A_DIM);
						}
						else if( tileCol == "YELLOW"){
							wattron(mapWin, COLOR_PAIR(4));
							waddch(mapWin, m_levelData[i][j]);
							wattroff(mapWin, COLOR_PAIR(4));
						}
						else if( tileCol == "GREEN"){
							wattron(mapWin, COLOR_PAIR(5));
							waddch(mapWin, m_levelData[i][j]);
							wattroff(mapWin, COLOR_PAIR(5));
						}
						else if( tileCol == "BLUE") {
							wattron(mapWin, COLOR_PAIR(6));
							waddch(mapWin, m_levelData[i][j]);
							wattroff(mapWin, COLOR_PAIR(6));
						}
						else if( tileCol == "RED") {
							wattron(mapWin, COLOR_PAIR(7));
							waddch(mapWin, m_levelData[i][j]);
							wattroff(mapWin, COLOR_PAIR(7));
						}
						else if( tileCol == "MAGENTA") {
							wattron(mapWin, COLOR_PAIR(8));
							waddch(mapWin, m_levelData[i][j]);
							wattroff(mapWin, COLOR_PAIR(8));
						}
						else if( tileCol == "CYAN") {
							wattron(mapWin, COLOR_PAIR(9));
							waddch(mapWin, m_levelData[i][j]);
							wattroff(mapWin, COLOR_PAIR(9));
						}
						else waddch(mapWin, m_levelData[i][j]);			// else draw map tile in standard color
					}
					
				}
                wattroff(mapWin, COLOR_PAIR(1));
            }
            else if((*fovInfo)[i][j].discovered) {	// not visible, but have we been here before?

				wattron(mapWin, COLOR_PAIR(2));
                waddch(mapWin, m_levelData[i][j]);			// yes, draw tile in grey on black
                wattroff(mapWin, COLOR_PAIR(2));                
            }
            // remove following else if, for debugging enemy fov only  ////////////
            //else if ((*enemyFov)[i][j].visible)  {		// do the same for enemies fov
				//if(m_enemyLayer[i][j] != ' ') {				// is current tile occupied by enemy?
					//waddch(mapWin, m_enemyLayer[i][j]);		// then draw its tile
				//}
				//else {
					//wattron(mapWin, COLOR_PAIR(3));
					//waddch(mapWin, m_levelData[i][j]);			// else draw map tile
					//wattroff(mapWin, COLOR_PAIR(3));
				//}
			//// end of enemy fov else if/////////////////////////////////////////
			//}
            else
            {
				waddch(mapWin, ' ');		// else: not visible and we've not yet discovered it: tile stays dark
			}
        }
    }
    wattroff(mapWin, A_BOLD);
}

void Level::movePlayer(const char input, Player* player, Interface &theInterface){

    LevelPoint playerPos;
	bool validInput = false;
	uint inputAttempts = 0;
    while(!validInput && player->getActionPoints() > 0 && !player->isDead() && inputAttempts < 3)
    {
    
		player->getPosition(playerPos);

		// this calculation is done in Level coordinates:
		//  o----> X
		//  | 
		//  |
		//  V 
		// Y
		
		switch(input) {
			case 'w':   // Up
			case 'W':
				playerPos.Y = playerPos.Y - 1;
				processPlayerMove(player, playerPos);
				validInput= true;
				break;
			case 's':   // Down
			case 'S':
				playerPos.Y = playerPos.Y + 1;
				processPlayerMove(player, playerPos);
				validInput= true;
				break;
			case 'a':   // Left
			case 'A':
				playerPos.X = playerPos.X - 1;
				processPlayerMove(player,playerPos);
				validInput= true;
				break;
			case 'd':   // Right
			case 'D':
				playerPos.X = playerPos.X + 1;
				processPlayerMove(player,playerPos);
				validInput= true;
				break;
			default:
				m_theInterface->addMsg(m_theInterface->commandWin, "\nINVALID INPUT!");
				m_theInterface->refreshComponent(m_theInterface->commandWin);
				player->setActionPoints(player->getActionPoints() - player->takeTurn(IDLE));
				inputAttempts++;
				break;
		}
		
		// give action point bonus to all entities when player is on tiles
		// with higher movementCost
		uint movementCost = m_tileInfo[playerPos.Y][playerPos.X].movementCost;
		if(movementCost > 100) {
			addActionPointBonuses(player, movementCost);
		}
    }
}


// update current speed modifiers according to ground tile
// this is different from the calculation for enemies: I want the player
// for each keypress to move into the desired direction. When calculating
// the same way like enemies (basically a penalty for being on a tile with
// higher movement costs), player would have to hit the directional key more
// than once for moving one step.
// instead, we turn the system around: when player is on a tile with a higher
// movement cost => all enemies get a bonus on their action points for this round
// example: player is on a tile with 150 movement cost => one step will cost 
// the player 100 action points and result in immediate move, but all enemies 
// around will get 50 action points extra => they might move twice every x steps 
// of the player. Sounds quirky, but works good enough for me.
void Level::addActionPointBonuses(const Player* player, const uint movementCosts) const {
	for (auto& entity : m_timedEntities) {	// loop through all timedEntities in the Level
			if (dynamic_cast<Player*>(entity) == nullptr) {		// but skip the player
				
				float entityMovementFactor = 1.0;
				
				// we need to scale the speed bonus when enemies are also affected by higher movementCost
				// only characters have basicSpeed and speed- all other timedEntity have speed only
				auto enemy = dynamic_cast<Enemy*>(entity);	// check if the current entity is an enemy
				if (enemy != nullptr) {		
					if (std::is_member_function_pointer<decltype(&Character::getBasicSpeed)>::value) {
						entityMovementFactor = enemy->getSpeed() / enemy->getBasicSpeed();    // scale the speed bonus
					}
				}

				uint speedBonus = (movementCosts - player->getSpeed()) * entityMovementFactor;
				uint entityAP = entity->getActionPoints();      // add the bonus to the entity´s action points
				entity->setActionPoints(entityAP + speedBonus);
			} 
		}
	}
	

void Level::updateEnemies(Player* player)
{
    char aiMove;
    LevelPoint playerPos;
    LevelPoint enemyPos;

    player->getPosition(playerPos);

    auto enemyIt = m_enemies.begin();	// iterate through all enemies
    while (enemyIt != m_enemies.end()) {
        auto& enemy = *enemyIt;			// get a handle for current enemy
        enemy->getPosition(enemyPos);
        		
			// ToDo: the following block is a duplication of the code at
			// the end of the while loop, reason: there are two ways/ places 
			// an enemy can be killed: during its own attack or during the
			// player attack. enemies marked as DEAD from the player´s turn get 
			// removed here
             if(enemy->getState() == DEAD) {
				enemy->getPosition(enemyPos);
				handleKilledEnemy(enemy, enemyPos, player);
                enemyIt = m_enemies.erase(enemyIt);
                break; }      
	
		if(!player->isDead()) {
			std::vector<std::vector<FoVInfo>>* enemyFov = enemy->getFoVPtr();
			if ((*enemyFov)[playerPos.Y][playerPos.X].visible) {
				enemy->m_moveQueue->clear();
				enemy->m_currentPath = enemy->pathfinder.FindPath(&m_tileInfo, enemyPos, playerPos);
				enemy->setMoveQueue(enemy->m_currentPath);
			}
        }
        else {
			enemy->m_moveQueue->clear();
		}

        while ((enemy->getActionPoints() > 0) && enemy->getState() != DEAD) {
            aiMove = enemy->getMove(playerPos);

            // Calculate new enemy position based on move
            LevelPoint newPos = enemyPos;
			switch (aiMove) {
				// trade off: additional checks add overhead, 
				// but I want to make absolutely sure an enemy move will not 
                // cause an out of bounds access to the map elements
                case 'w':
                    newPos.Y = std::max(static_cast<int>(enemyPos.Y) - 1, 0); // Move up
                    break;
                case 's':
                    newPos.Y = std::min(enemyPos.Y + 1, m_mapSize.Height - 1);  // Move down
                    break;
                case 'a':
                    newPos.X = std::max(static_cast<int>(enemyPos.X) - 1, 0); // Move left
                    break;
                case 'd':
                    newPos.X = std::min(enemyPos.X + 1, m_mapSize.Width - 1);   // Move right
                    break;
				}
            // Process enemy move and update position
            processEnemyMove(player, enemy, newPos);
			
			// handle dead monsters after their turn
            if(enemy->getState() == DEAD) {
				enemy->getPosition(enemyPos);
				handleKilledEnemy(enemy, enemyPos, player);
                enemyIt = m_enemies.erase(enemyIt);
                break;
            }
            updateMap(m_theInterface->map, player->getFoVPtr());
            
            // update current speed modifiers according to ground tile
			float movementFactor = 100.0 / (float) m_tileInfo[enemyPos.Y][enemyPos.X].movementCost;
			uint newSpeed = enemy->getBasicSpeed() * movementFactor; 
			enemy->setSpeed(newSpeed);
        }

        if (enemy->getState() != DEAD) {
            ++enemyIt;
        }
    }
}

void Level::handleKilledEnemy(std::shared_ptr<Enemy> enemy, LevelPoint enemyPos, Player* player)
{
    enemy->getPosition(enemyPos);
    m_enemyLayer[enemyPos.Y][enemyPos.X] = ' '; // delete monster sprite in enemy map layer
    updateMap(m_theInterface->map, player->getFoVPtr());
    m_theInterface->refreshComponent(m_theInterface->mapWin);	 // redraw Level
    m_theInterface->addGameLog("%s died! ", enemy->getName().c_str());	// add memo to gamelog
    m_theInterface->refreshComponent(m_theInterface->gameLog);
    enemy->removeFromTimekeeper(m_timedEntities);	// also remove from m_timedEntities queue
    m_theInterface->clearWin(m_theInterface->enemyStatus);	// finally clear the enemy status window
}

char Level::getTile(const LevelPoint pos) const{
    if((pos.X >= 0 ) && (pos.Y >= 0) && (pos.X < m_mapSize.Width) && (pos.Y < m_mapSize.Height)){
        return m_levelData[pos.Y][pos.X];
    }
    else{
        return '-';
    }
}

// get object on Tile x,y from enemy map layer
char Level::getEnemyTile(const LevelPoint pos) const{
	if((pos.X >= 0 ) && (pos.Y >= 0) && (pos.X < m_mapSize.Width) && (pos.Y < m_mapSize.Height)){
        return m_enemyLayer[pos.Y][pos.X];
    }
    else{
        return '-';
    }
}

// returns true/1 if translucent, false/0 if blocks light
bool Level::getTranslucent(const LevelPoint pos) const{
    return m_tileInfo[pos.Y][pos.X].translucent;
}

// opposite getter of getTranslucent (true/1 if blocks light, false/0 if translucent)
bool Level::getBlocksLight(const LevelPoint pos) const{
    return !m_tileInfo[pos.Y][pos.X].translucent;
}

MapSize Level::getMapSize() const{
    return m_mapSize;
}

// get Enemy handle for ListPos in m_enemies 
std::shared_ptr<Enemy>* Level::getEnemy(unsigned int ListPos){
	std::shared_ptr<Enemy>* theEnemy = &m_enemies[ListPos];
	theEnemy = &m_enemies[ListPos];
	return theEnemy;
}

//get Nr. of enemies currently in m_enemies
uint Level::getNrEnemies() { return m_enemies.size(); }

// get handle for m_tileInfo
const std::vector<std::vector<TileInfo>>* Level::getTileInfo() const { return &m_tileInfo; }


// Getter function for m_timedEntities
const std::deque<TimedEntity*>& Level::getTimedEntitiesPtr() const {
    return m_timedEntities;
  }


// get handle for FoV matrix
std::vector<std::vector<FoVInfo>>* Level::getFoVPtr() {return &m_playerFoVInfo;}

// getter for Level start position
LevelPoint Level::getLadderUpPosition() { return m_ladderUpPosition;}

// getter for Level end position
LevelPoint Level::getLadderDownPosition() { return m_ladderDownPosition; }
	
////////////////////////////////////////////////////////////////////////
// Setters
////////////////////////////////////////////////////////////////////////

void Level::setTile(LevelPoint pos, char tile){
    m_levelData[pos.Y][pos.X] = tile;
} 

void Level::setPlayerFoVInfo(std::vector<std::vector<FoVInfo>> &playerFoVInfo) {
	m_playerFoVInfo = playerFoVInfo;
}
    
void Level::processPlayerMove(Player* player, LevelPoint targetPos){
    
    LevelPoint playerPos;

    if(player->isDead()){
        m_theInterface->addGameLog("You are dead!");
        return;
    }
	
    player->getPosition(playerPos);
	
	 //first check if there is an enemy on the target position
	char targetTile = m_enemyLayer[targetPos.Y][targetPos.X];	// get current tile in enemy map layer
	int enemyListPos = cfg_EnemiesDataPos(targetTile, m_enemiesConfig);	// check if this is an enemy represented in the parsed config file
	bool targetTileBlocks = m_tileInfo[targetPos.Y][targetPos.X].blocks;
	EntityAction action = IDLE;
	
	if(targetTileBlocks)	// can player move to new tile?
	{
		m_theInterface->addGameLog(m_tileInfo[targetPos.Y][targetPos.X].description.c_str());
		player->setActionPoints(player->getActionPoints() - player->takeTurn(action));
		return;		// no, we hit a wall or similiar -> abort here
	}
	else if(enemyListPos != -1) {	// is there an enemy on the target tile?
		battleEnemy(player, targetPos);		// yes, attack!
		action = ATTACK;
		player->setActionPoints(player->getActionPoints() - player->takeTurn(action));
		return;
	}
	else if(enemyListPos == -1) {	// here is clear: we didn't hit a wall nor an enemy => move to tile
		m_enemyLayer[playerPos.Y][playerPos.X] = ' ';	// clear player pos in enemy layer
		player->setPosition(targetPos);					// update player object with new position
		m_enemyLayer[targetPos.Y][targetPos.X] = '@';	// also update player pos in enemy layer
		action = MOVE;
		player->setActionPoints(player->getActionPoints() - player->takeTurn(action));
	}
	else {
		return;
	}
}

void Level::processEnemyMove(Player* player, std::shared_ptr<Enemy> enemy, LevelPoint targetPos) {
	LevelPoint playerPos;
	LevelPoint enemyPos;
	enemy->getPosition(enemyPos);
	player->getPosition(playerPos);

	EntityAction action = IDLE;

	//first check if there is another enemy or the player on the target position
	char targetTile = m_enemyLayer[targetPos.Y][targetPos.X];    // get current tile in enemy map layer
	int enemyListPos = cfg_EnemiesDataPos(targetTile, m_enemiesConfig);    // check if this is an enemy represented in the parsed config file
	bool targetTileBlocks = m_tileInfo[targetPos.Y][targetPos.X].blocks;

	if (targetTileBlocks || (enemyListPos != -1) || targetTile == 't') {    // can enemy move to new tile?
		enemy->setActionPoints(enemy->getActionPoints() - enemy->takeTurn(action));    // action is IDLE- substract all action points in this round
		return;        // no, we hit a wall or similiar, or there's another monster, or players corpse at the target tile -> abort here
	}
	// TODO: @ is hardcoded here, make configurable
	else if (targetTile == '@') {    // is there a player on the target tile?
		battleEnemy(player, enemyPos);        // yes, attack!
		action = ATTACK;
		enemy->setActionPoints(enemy->getActionPoints() - enemy->takeTurn(action));
		return;
	}
	else {    // here is clear: we didn't hit a wall nor an enemy => move to tile
		m_enemyLayer[enemyPos.Y][enemyPos.X] = ' ';        // clear enemy pos in enemy layer
		enemy->setPosition(targetPos);    // update enemy object with new position
		m_enemyLayer[targetPos.Y][targetPos.X] = enemy->getTile();    // also update enemy pos in enemy layer
		action = MOVE;
		enemy->setActionPoints(enemy->getActionPoints() - enemy->takeTurn(action));

		bool playerSeesEnemy = player->isInFoV(targetPos);
		if (playerSeesEnemy && !enemy->getWasSeenPlayer()) {
			enemy->setWasSeenPlayer(true);
			m_theInterface->PLAYER_SEES("%s", enemy->getRandomDescription().c_str());
		}
		//m_theInterface->addDebugMsg("has seen: %i", enemy->getHasSeenPlayer());

		bool enemySeesPlayer = enemy->isInFoV(playerPos);
		if (enemySeesPlayer && !(enemy->getHasSeenPlayer())) {
			m_theInterface->PLAYER_HEARS("%s", enemy->getBattleCry().c_str());
			enemy->setHasSeenPlayer(true);
		}
	}
	return;
}

void Level::battleEnemy(Player* player, LevelPoint targetPos){

    LevelPoint enemyPos;
    LevelPoint playerPos;

    int attackRoll;
    int attackResult;
    std::string enemyName;

    player->getPosition(playerPos);

    for(unsigned int i = 0; i < m_enemies.size(); i++){       // loop through all enemies
        m_enemies[i]->getPosition(enemyPos);    // get current enemies position
        enemyName = m_enemies[i]->getName();
        if(targetPos.X == enemyPos.X && targetPos.Y == enemyPos.Y){

            // BATTLE!
            m_theInterface->updateEnemyStatus(enemyName.c_str(),         // update enemies stats in interface window
                                             m_enemies[i]->getLevel(), m_enemies[i]->getAttack(),
                                             m_enemies[i]->getDefense(), m_enemies[i]->getHealth());

            attackRoll = player->attack();   // Player´s turn: roll the attack dice!
			attackRoll = attackRoll * m_tileInfo[playerPos.Y][playerPos.X].battleModifiers.attack;		// consider map tile modifiers
			
            m_theInterface->addGameLog("Player attacks %s with a roll of %d  ", enemyName.c_str(), attackRoll);
            m_theInterface->refreshComponent(m_theInterface->gameLog);
            attackResult = m_enemies[i]->takeDamage(attackRoll, m_tileInfo[enemyPos.Y][enemyPos.X].battleModifiers.defence);	// consider map tile modifier when calculating attack result

            if(attackResult != 0){      // The monster died    
                m_enemies[i]->setActionPoints(0);	// NOTE: this code used to handle the removal of the enemy as well
				m_enemies[i]->setState(DEAD);	// which lead to random segfaults. now the removal part is done in updateEnemies()
				
				
                if(player->addExperience(attackResult) == TRUE){
                    m_theInterface->addGameLog("Player leveled up!");
                }
                return;
            }

            // Monsters turn
            attackRoll = m_enemies[i]->attack();
			attackRoll = attackRoll * m_tileInfo[enemyPos.Y][enemyPos.X].battleModifiers.attack;		// consider map tile modifiers
            m_theInterface->addGameLog("%s attacks Player with a roll of %d  ", enemyName.c_str(), attackRoll);
            m_theInterface->refreshComponent(m_theInterface->gameLog);

            attackResult = player->takeDamage(attackRoll, m_tileInfo[playerPos.Y][playerPos.X].battleModifiers.defence); 	// consider map tile modifier when calculating attack result

            if(attackResult != 0){
                setTile(playerPos, 't');     // replace player sprite with cross
                m_enemyLayer[playerPos.Y][playerPos.X] = 't';
                updateMap(m_theInterface->map, player->getFoVPtr());      // redraw Level
                m_theInterface->mapWin.refresh();
                player->playerDied();
                player->removeFromTimekeeper(m_timedEntities);

                m_theInterface->addGameLog("You were killed by %s! ", enemyName.c_str());
            }
            return;
        }
    }
    return;
}
