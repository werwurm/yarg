// Utility functions
#pragma once

#include "Datatypes.h"

uint getDistance(LevelPoint P1, LevelPoint P2);		// L0 norm distance, fastest but leads to artifacts in FoV
uint getDistance2(LevelPoint P1, LevelPoint P2);	// euclidean distance, slower but FoV makes more sense
float FastSqrt(float x); 	// the famous fast inverse square root from Quake II
