#pragma once

#include <functional>
#include <algorithm>
#include <cmath>
#include <cstring>
#include <cstddef>
#include "Datatypes.h"


class Visibility {
public:
    virtual void Compute(std::vector<std::vector<FoVInfo>>* fovInfo, LevelPoint origin, unsigned int rangeLimit) = 0;
    void SetAllInvisible(std::vector<std::vector<FoVInfo>>* fovInfo);     // init whole map as invisible
	void SetAllVisible(std::vector<std::vector<FoVInfo>>* fovInfo);     // set whole map as visible and discovered
	void SetAllUndiscovered(std::vector<std::vector<FoVInfo>>* fovInfo);	// set whole map as undiscovered
      
protected:
    Level *m_level;
    MapSize m_mapSize;    
};

// classical RayCasting algorithm, fast and reliable
class RayCastVisibility : public Visibility{
public:
    RayCastVisibility(Level *level);
    
    void Compute(std::vector<std::vector<FoVInfo>>* fovInfo, LevelPoint origin, unsigned int rangeLimit) override;

    struct Rectangle {      // used by RayCast algorithm
        int X;
        int Y;
        int Width;
        int Height;
        Rectangle(int x, int y, int width, int height) : X(x), Y(y), Width(width), Height(height) {}      
        // calculate intersection of the object with the rectangle given as argument
        // modify the object to the dimensions of the intersection
        void Intersect(const Rectangle& other) {
            int x1 = std::max(X, other.X);  // upper left of the intersection
            int y1 = std::max(Y, other.Y);  
            int x2 = std::min(Right(), other.Right());  // lower right of intersection
            int y2 = std::min(Bottom(), other.Bottom());
            // modify object to new dimensions
            X = x1;
            Y = y1;
            Width = std::max(0, x2 - x1);
            Height = std::max(0, y2 - y1);
        }
        int Left() const { return X; }
        int Top() const { return Y; }
        int Right() const { return X + Width; }
        int Bottom() const { return Y + Height; }
    };    
    
private:  
    void TraceLine(std::vector<std::vector<FoVInfo>>* fovInfo, LevelPoint origin, int x2, int y2, unsigned int rangeLimit);
};

class MyVisibility : public Visibility{
public: 
    MyVisibility(Level *level);
    
    void Compute(std::vector<std::vector<FoVInfo>>* fovInfo, LevelPoint origin, unsigned int rangeLimit) override;
    
    struct Slope {     // represents the slope Y/X as a rational number
        uint Y;
        uint X;
        Slope(uint y, uint x) {
            Y = y;
            X = x;
        }
        bool Greater(uint y, uint x) {
            return Y * x > X * y;
        }
        bool GreaterOrEqual(uint y, uint x) {
            return Y * x >= X * y;
        }
        bool Less(uint y, uint x) {
            return Y * x < X * y;
        }
    };  
    
private:
    //void ComputeOctant(unsigned int octant, LevelPoint origin, unsigned int rangeLimit, unsigned int x, Slope top, Slope bottom);    
    //bool BlocksLight(uint x, uint y, uint octant, LevelPoint origin);
    //void SetVisible(unsigned int x, unsigned int y, unsigned int octant, LevelPoint origin);
};
