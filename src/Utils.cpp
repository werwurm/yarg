#include "Utils.h"
#include "Datatypes.h"

// L0 norm, understates the diagonal distances
uint getDistance(LevelPoint P1, LevelPoint P2){
    int dx = P2.X - P1.X;
    int dy = P2.Y - P1.Y;
    
    unsigned int dist = abs(dx) + abs(dy);
    return dist;
}

// L2/ Euclidean Distance using fast inverse square root
uint getDistance2(LevelPoint P1, LevelPoint P2){
    int dx = P2.X - P1.X;
    int dy = P2.Y - P1.Y;

   unsigned int dist = (unsigned int) FastSqrt(dx * dx + dy * dy);
   return dist;
}

// the famous fast inverse square root from Quake II
float FastSqrt(float x) {
    float xhalf = 0.5f * x;
    int i = *(int*)&x;         // get the integer representation of x
    i = 0x5f3759df - (i >> 1);  // initial guess for Newton's method
    x = *(float*)&i;           // convert back to floating-point number
    x = x * (1.5f - xhalf * x * x);  // one iteration of Newton's method
    return 1.0f / x;           // return the reciprocal to get the square root
}
