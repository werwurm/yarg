#include <vector>
#include <string>
#include <random>

#include "Character.h"
#include "TimedEntities.h"

// Constructor
Character::Character(char tile, std::string name, uint level, int health, uint attack, uint defense, uint visRange, std::vector<std::string> descriptions, std::vector<std::string> battleCries, uint speed)
    : TimedEntity(speed), m_tile(tile), m_name(name), m_level(level), m_health(health), m_attack(attack), m_defense(defense),
    m_visibilityRange(visRange), m_descriptions(descriptions), m_battleCries(battleCries), m_moveQueue(new std::vector<LevelPoint>()) {
		
    m_tile = tile;
    m_name = name; 
    m_level = level;
    m_health = health;
    m_attack = attack;
    m_defense = defense;
    m_visibilityRange = visRange;
    m_descriptions = descriptions;
    m_battleCries = battleCries;
    m_moveQueue = new std::vector<LevelPoint>(); // Allocate memory for the movement vector
    
    m_basicSpeed = speed;
}

Character::~Character() {
    delete m_moveQueue;
}

// Getter methods
void Character::getPosition(LevelPoint &pos) const{
    pos.X = m_position.X;
    pos.Y = m_position.Y;
}

std::string Character::getName() const { return m_name; }
char Character::getTile() const { return m_tile; }
uint Character::getLevel() const { return m_level; }
uint Character::getAttack() const { return m_attack; }
uint Character::getDefense() const { return m_defense; }
int Character::getHealth() const { return m_health; }
uint Character::getVisRange() const { return m_visibilityRange;}
std::vector<std::vector<FoVInfo>>* Character::getFoVPtr() {return &m_fovInfo;}
std::vector<LevelPoint>* Character::getMoveQueuePtr() const { return m_moveQueue; }
bool Character::getWasSeenPlayer() const { return m_wasSeenByPlayer; }
bool Character::getHasSeenPlayer() const { return m_hasSeenPlayer; }

// return randomly one of the descriptions of the character
std::string Character::getRandomDescription() const {	
	if (m_descriptions.empty()) {
            return "no description here"; // handle empty vector case
        }

    // create a random engine and distribution
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, m_descriptions.size() - 1);

    // generate a random index and return the corresponding element
    return m_descriptions[dis(gen)];
	
}

// return randomly one of the battle cries of the monster
std::string Character::getBattleCry() const {	
	if (m_battleCries.empty()) {
            return "I have no battle cry!"; // handle empty vector case
        }

    // create a random engine and distribution
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, m_battleCries.size() - 1);

    // generate a random index and return the corresponding element
    return m_battleCries[dis(gen)];
	
}

// given position is visible for character
bool Character::isInFoV(LevelPoint &pos) const {
    if (pos.Y >= m_fovInfo.size() || pos.X >= m_fovInfo[0].size()) {
        // pos is out of bounds, so it can't be in the FoV
        return false;
    }
    
    return m_fovInfo[pos.Y][pos.X].visible;
}

EntityState Character::getState() const { return m_state; }

uint Character::getBasicSpeed() const {return m_basicSpeed; }

// Setters
void Character::setPosition(LevelPoint pos){
    m_position.X = pos.X;
    m_position.Y = pos.Y;
}

// TODO: check if this should go into player and enemy classes
void Character::setWasSeenPlayer(bool wasSeen) { m_wasSeenByPlayer = wasSeen;}
void Character::setHasSeenPlayer(bool hasSeen) { m_hasSeenPlayer = hasSeen;}


void Character::setVisRange(uint range) { m_visibilityRange = range; }

// get handle for movement Queue vector
void Character::setMoveQueue(const std::vector<LevelPoint>& newMoveQueue) {
    if (m_moveQueue == nullptr) { // Check if _moveQueue is nullptr
        m_moveQueue = new std::vector<LevelPoint>(); // Initialize _moveQueue with a new vector object
    }
    *m_moveQueue = newMoveQueue; // Assign newMoveQueue to the vector pointed to by _moveQueue
}

void Character::setState(EntityState newState) { m_state = newState;}
void Character::setBasicSpeed(uint basicSpeed) { m_basicSpeed = basicSpeed; }

// set the private member m_fovInfo to pointer given as argument
void Character::setFoVPtr(const std::vector<std::vector<FoVInfo>>& fovInfo) {
    m_fovInfo = fovInfo;
}
