#include <ctime>
#include <iostream>

#include "GameSystem.h"
#include "Screen.h"
#include "Frame.h"
#include "ConfigParser.h"

GameConfig gameConfig;	// make this config global

int main()
{
    // initialize ncurses
    Screen scr;
    
    std::srand(std::time(nullptr));  // seed the random number generator with current time
	uint seed = std::rand();

	// load game config, Monsters and map configuration
	gameConfig = cfg_readGameConfig("settings.cfg");
	//cfg_printConfigData(gameConfig);
	//exit(0);
	
	if(gameConfig.seed == 0) {
		gameConfig.seed = seed;		// if configured to use random seed -> set the seed for this run
	}
	
	std::vector<EnemyData> enemiesData = cfg_readEnemiesConfig(gameConfig.EnemyConfigFile);
	std::vector<MapData> mapData = cfg_readMapConfig(gameConfig.MapConfigFile);

    // create game instance, load level design
    GameSystem gameSystem(&enemiesData, &mapData);
    gameSystem.playGame();
    
    // End of game
    endwin();
	std::cout << "Your Seed was: " << gameConfig.seed << std::endl;
	return 0;
}


