#pragma once
#include <random>

#include "ConfigParser.h"
#include "Datatypes.h"

#define NR_ENEMIES 5

class MapGenerator
{
public:
    std::vector<std::string> CellularAutomata(uint seed, int width, int height, int iterations, int percentAreWalls, const std::vector<MapData>* mapDataPtr);
    std::vector<std::string>DrunkardsWalk(uint seed, int width, int height, int pathLength, int percentareWalls, bool startcenter, const std::vector<MapData>* mapDataPtr);
    std::vector<std::string> PlaceEnemies(std::vector<std::string>& map, uint seed, const std::vector<MapData>* mapDataPtr);
    
    void connectIsolatedAreas(std::vector<std::string>& map, const std::vector<MapData>* mapDataPtr);

    // Post processing methods
    void AddLakes(std::vector<std::string>& map, unsigned int seed, int numLakes, int minLakeSize, int maxLakeSize, const std::vector<MapData>* mapDataPtr);
    void AddMoss(std::vector<std::string>& map, unsigned int seed, uint mossWallProbability, uint mossWaterProbability, uint mossWaterWallProbability, const std::vector<MapData>* mapDataPtr);
    void AddTrees(std::vector<std::string>& map, unsigned int seed, uint treeProbability, uint treeNeighbourProbability,  const std::vector<MapData>* mapDataPtr);
    void AddGates(std::vector<std::string>& map, unsigned int seed, uint wallReplacementProb, uint wallWaterReplacementProb, const std::vector<MapData>* mapDataPtr);
    static void FixBorders(std::vector<std::string>& map, const std::vector<MapData>* mapDataPtr);
    
    void PlaceObjects(std::vector<std::string>& map, uint seed, const std::vector<MapData>* mapDataPtr);

private:
    // Cellular Automata map generator helper methods
    static void RandomFill(std::vector<std::string>& map, uint width, uint height, uint percentAreWalls, std::mt19937& mt, char wallTile, char floorTile);
    static void Step(std::vector<std::string>& map, int width, int height, char wallTile, char floorTile);
    static char PlaceWallLogic(std::vector<std::string>& map, int width, int height, int x, int y, char wallTile, char floorTile);
    static int CountAdjacentWalls(std::vector<std::string>& map, int width, int height, int x, int y, char wallTile);
    static int CountNearbyWalls(std::vector<std::string>& map, int width, int height, int x, int y, char wallTile);
	
	// Drunkards Walk map generator helper methods
	int getRandomDirection();
	
    // connectIsolatedAreas helper methods
    static void floodFill(std::vector<std::string>& map, int row, int col, char target, char replacement);
    static std::vector<std::pair<int, int>> findIsolatedAreas(std::vector<std::string>& map, char floorTile, char wallTile);
    static void markConnectedAreas(std::vector<std::string>& map, char floorSymbol, char markSymbol);
    static void resetFloor(std::vector<std::string>& map, const std::vector<MapData>* mapDataPtr);

    // post processing methods

};

