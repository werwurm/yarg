#pragma once
#include "Datatypes.h"
#include "Character.h"

class Player:public Character{
public: 
    Player(char tile, std::string name, uint level, int health, uint attack, uint defense, uint visRange, std::vector<std::string> descriptions, std::vector<std::string> battleCries, uint experience, uint speed);
    
    int attack();       // calculate random attack value based on attack number
    int takeDamage(int attack, float defenceModifier);    // calculate damage and reduce health, return 1 if player died
	
	virtual int takeTurn(EntityAction action) override;	// return the consumed action points

	
	// Setters
    bool addExperience(uint xp);
    void playerDied();
    
    // Getters
    uint getExperience() const;
    bool isDead() const;
    
    friend class Level;		// Level needs access to the fovInfo data
    friend class GameSystem;
    
private:    
    // additional properties for players
    uint m_experience;    
};
