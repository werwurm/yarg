#include <random>

#include "Player.h"

Player::Player(char tile, std::string name, uint level, int health, uint attack, uint defense, uint visRange, std::vector<std::string> descriptions, std::vector<std::string> battleCries, uint experience, uint speed)
  : Character(tile, name, level, health, attack, defense, visRange, descriptions, battleCries, speed), m_experience(experience){}

  
int Player::attack(){
    static std::default_random_engine randomEngine(time(NULL));  // set up RNG based on time, so every run will be different
    std::uniform_int_distribution<int> attackRoll(0, m_attack);   // pick a random number between 0 and the attack number of player

    return attackRoll(randomEngine);
}

int Player::takeDamage(int attack,  float defenceModifier){
	attack = attack - m_defense * defenceModifier;
    // Check if attack does damage
    if(attack > 0) {
        m_health -= attack;  // it did, reduce players health
        // check if Player died
        if(m_health <= 0){
            return 1;
        }
    }
    else
        return 0;      // monster is still alive, if attack did damage monster´s health is now reduced

    return 0;
}  
  
// return the consumed action points
int Player::takeTurn(EntityAction action) {
    return 100;
}
  
// Setters
// add experience and level up if applicable
bool Player::addExperience(uint xp){
    m_experience += xp;

    bool levelUp = false;

    while(m_experience > 50){    // Level Up
        levelUp = true;
        m_experience -= 50;
        m_attack += 10;
        m_defense += 5;
        m_health += 10;
        m_level++;
    }
    return levelUp;
}

void Player::playerDied(){
    m_state = DEAD;
    m_tile = 't';    // change player tile from '@' to 't'
}

// Getters
uint Player::getExperience() const{ return m_experience;}

bool Player::isDead() const {
	if(m_state == DEAD) {
		return true;
	}
	else {
		return false;
	}
}
