// Collection of debug macros
#pragma once

//#define DEBUG_CONSOLE   // comment out for no debug console

// variadic macro that expands into calling the interface.addDebugMsg
// example usage: LOG_ENGINE_DEBUG("Player: (%i,%i)", 15,13);
#ifdef DEBUG_CONSOLE
    #define LOG_ENGINE_DEBUG(fmt, ...) do { \
        char buf[256]; \
        std::snprintf(buf, 256, fmt, ##__VA_ARGS__); \
        interface.addDebugMsg(buf); \
    } while (0)
#else
#define LOG_ENGINE_DEBUG(fmt, ...) (void())
#endif
