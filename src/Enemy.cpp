#include <random>
#include "Enemy.h"
#include "Datatypes.h"

Enemy::Enemy(char tile, std::string name, uint level, int health, uint attack, uint defense, uint visRange, std::vector<std::string> descriptions, std::vector<std::string> battleCries, uint xp, uint speed) 
	:Character(tile, name, level, health, attack, defense, visRange, descriptions, battleCries, speed), m_experienceValue(xp) {}

uint Enemy::attack(){
    static std::default_random_engine randomEngine(time(NULL));  // set up RNG based on time, so every run will be different
    std::uniform_int_distribution<int> attackRoll(0, m_attack);   // pick a random number between 0 and the attack number of player

    return attackRoll(randomEngine);
}

int Enemy::takeDamage(int attack, float defenceModifier){
	attack = attack - m_defense * defenceModifier;
	
    // Check if attack does damage
    if(attack > 0) {
        m_health -= attack;  // it did, reduce monsters health
        // check if monster died
        if(m_health <= 0){
            return m_experienceValue;
        }
    }
    else
        return 0;      // monster is still alive, if attack did damage monster´s health is now reduced
    return 0;
}

// return the consumed action points
int Enemy::takeTurn(EntityAction action) { 
	switch(action){
		case IDLE:
			return this->getSpeed();		// idling should cost all action points in this round
			break;
		case MOVE:
			return 100;		// 100 action points per round is default. enemies with higher speeds get more action points per round => can potentially move more than once
			break;
		case ATTACK:
		return this->getActionPoints();
			//return this->getSpeed();	// ATTACK should always take full action points (the equivalent of speed) for one round
			break;
		default:
		return 100;
	}
	return 100; 	// should never be reached
} 

char Enemy::getMove(LevelPoint playerPos){
    
    if(this->getActionPoints() < 0){
		return('.');
	}
    
    static std::default_random_engine randEngine(time((NULL)));
    std::uniform_int_distribution<int> moveRoll(0,6);
	
	bool followingPath = false;
	LevelPoint targetPos;

	if (m_moveQueue->size() != 0) {		// check if enemy has a current movement Queue (other option would be moving randomly
		targetPos = (*m_moveQueue)[0];	// yep, take the first point
		if( (targetPos.X == m_position.X) && (targetPos.Y == m_position.Y)  && m_moveQueue != nullptr) {
			m_moveQueue->erase(m_moveQueue->begin());	// if first target tile is identical with current pos -> take the next one
			targetPos = (*m_moveQueue)[0];
		}
		m_moveQueue->erase(m_moveQueue->begin());	// then delete it from the Queue
		followingPath = true;
	}
	else
	{
		targetPos.X = playerPos.X;
		targetPos.Y = playerPos.Y;
	}
	
    int dx = m_position.X - targetPos.X; // distance in x direction
    int dy = m_position.Y - targetPos.Y; // distance in y direction
    
    int adx = abs(dx);
    int ady = abs(dy);
    
    //bool playerVisible = (*enemyFov)[playerPos.Y][playerPos.X].visible;		
	bool playerVisible = isInFoV(playerPos);	// check if player is in FoV
	
    // should enemy move to player? (because it sees player)
    if( playerVisible || followingPath) {		// player is in FoV or enemy is following a path from movement Queue
        if(adx > ady) {     // move along x axis
            if(dx > 0) {
                return 'a';    // move left
            }
            else {
                return 'd';     // move right
            }
        } else {    // move along y axis
            if(dy > 0) {    // move down
                return 'w';
            }
            else {
                return 's'; // move up
            }
        }
    }

    // if enemy doesn't see player or has no current path, move randomly
    int randomMove = moveRoll(randEngine);
    switch(randomMove) {
        case 0:
            return 'a';
        case 1:
            return 'w';
        case 2:
            return 's';
        case 3:
            return 'd';
        default:        // for 4 to 6 do nothing
            return '.';
    }
}
