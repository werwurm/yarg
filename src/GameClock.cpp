#include <iostream>
#include <iomanip>
#include "GameClock.h"


int GameClock::takeTurn(EntityAction action) {
    return 0;
}

std::string GameClock::getTimeString() const {
	const size_t timeOffset = 43200;
    size_t totalSeconds = timeOffset + getActionPoints() / 25; // each round equals 100 action points equals 4s game time 
    uint days = totalSeconds / 86400;
    uint hours = (totalSeconds % 86400) / 3600;
    uint minutes = (totalSeconds % 3600) / 60;
    //uint seconds = totalSeconds % 60;
    std::stringstream ss;

    if (days > 0) {
        ss << days << "d ";
    }
    ss << std::setfill('0') << std::setw(2) << hours << ":";
    ss << std::setfill('0') << std::setw(2) << minutes;// << ":";
    //ss << std::setfill('0') << std::setw(2) << seconds;
    return ss.str();
}

uint GameClock::getNrRounds() const {
	return (getActionPoints() / 100); // player has per default 100 ap per round
}
