#pragma once
#include <string>

#include "Datatypes.h"
#include "Character.h"
#include "Dijkstra.h"

class Enemy:public Character
{
public:	
    Enemy(char tile, std::string name, uint level, int health, uint attack, uint defense, uint visRange, std::vector<std::string> descriptions, std::vector<std::string> battleCries, uint xp, uint speed);
    
    uint attack();       // calculate random attack value based on attack number
    int takeDamage(int attack, float defenceModifier);    // calculate damage and reduce health, return 1 if player died
    
	virtual int takeTurn(EntityAction action) override;		// return the consumed action points
    
    char getMove(LevelPoint playerPos);      // get AI move command
    
    friend class Level;		// Level needs access to the fovInfo data

	Dijkstra pathfinder;	// object for AI pathfinding
	
private:    
    // additional properties for enemies  
    uint m_experienceValue;
    std::vector<LevelPoint> m_currentPath;
};
