#include <algorithm>
#include <climits>
#include <vector>
#include <iostream>

#include "MapGenerator.h"
#include "ConfigParser.h"
#include "Dijkstra.h"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Cellular Automata and helper functions for map generation
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
std::vector<std::string> MapGenerator::CellularAutomata(uint seed, int width, int height, int iterations, int percentAreWalls, const std::vector<MapData>* mapDataPtr) {
    
    const char wallTile = cfg_getMapTileByName(*mapDataPtr, "wall");
    const char floorTile = cfg_getMapTileByName(*mapDataPtr, "floor");
    
    if(wallTile == '?'){
		printf("\nError: no tile named \"wall\" found in map config\n");
		exit(0);
	}
	else if(floorTile == '?') {
		printf("\nError: no tile named \"floor\" found in map config\n");
		exit(0);
	}	
    
    std::vector<std::string> map(height, std::string(width, floorTile));

    std::mt19937 mt(seed);

    RandomFill(map, width, height, percentAreWalls, mt, wallTile, floorTile);

    for(int i = 0; i < iterations; i++){
        //map = Step(map, width, height, wallTile, floorTile);
        Step(map, width, height, wallTile, floorTile);
	}
    return map;
}

void MapGenerator::RandomFill(std::vector<std::string>& map, uint width, uint height, uint percentAreWalls, std::mt19937& mt, char wallTile, char floorTile) {
    std::uniform_int_distribution<int> randomColumn(4, width - 4);

    for(uint y = 0; y < height; y++) {
        for(uint x = 0; x < width; x++) {
            if(x == 0 || y == 0 || x == width - 1 || y == height - 1)
                map[y][x] = wallTile;
            else if(x != (uint) randomColumn(mt) && (uint) mt() % 100 < percentAreWalls)
                map[y][x] = wallTile;
            else
                map[y][x] = floorTile;
        }
    }
}

void MapGenerator::Step(std::vector<std::string>& map, int width, int height, char wallTile, char floorTile) {
    std::vector<std::string> newMap(height, std::string(width, floorTile));

    for(int y = 0; y < height; y++) {
        for(int x = 0; x < width; x++) {
            if(x == 0 || y == 0 || x == width - 1 || y == height - 1)
                newMap[y][x] = wallTile;
            else
                newMap[y][x] = PlaceWallLogic(map, width, height, x, y, wallTile, floorTile);
        }
    }
	
	map = newMap;
}

char MapGenerator::PlaceWallLogic(std::vector<std::string>& map, int width, int height, int x, int y, char wallTile, char floorTile) {
    if(CountAdjacentWalls(map, width, height, x, y, wallTile) >= 5 || CountNearbyWalls(map, width, height, x, y, wallTile) <= 2)
        return wallTile;
    else
        return floorTile;
}

int MapGenerator::CountAdjacentWalls(std::vector<std::string>& map, int width, int height, int x, int y, char wallTile) {
    int walls = 0;

    for(int mapX = x - 1; mapX <= x + 1; mapX++) {
        for(int mapY = y - 1; mapY <= y + 1; mapY++) {
            if( (mapX < 0 || mapX >= width || mapY < 0 || mapY >= height)) {
                continue;
            }
            if(map[mapY][mapX] == wallTile)
                walls++;
        }
    }

    return walls;
}

int MapGenerator::CountNearbyWalls(std::vector<std::string>& map, int width, int height, int x, int y, char wallTile)
{
    int walls = 0;
    for(int mapX = x - 2; mapX <= x + 2; mapX++)
    {
        for(int mapY = y - 2; mapY <= y + 2; mapY++)
        {
            if(abs(mapX - x) == 2 && abs(mapY - y) == 2) continue;
            if(mapX < 0 || mapY < 0 || mapX >= width || mapY >= height) continue;
            if(map[mapY][mapX] == wallTile) walls++;
        }
    }
    return walls;
}
//// End of Cellular Automata map generator functions

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Drunkards Walk algorithm and helper functions for map generation
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
std::vector<std::string> MapGenerator::DrunkardsWalk(uint seed, int width, int height, int pathLength, int percentareWalls, bool startcenter, const std::vector<MapData>* mapDataPtr) {
    
    const char wallTile = cfg_getMapTileByName(*mapDataPtr, "wall");
    const char floorTile = cfg_getMapTileByName(*mapDataPtr, "floor");
    
    if(wallTile == '?'){
		printf("\nError: no tile named \"wall\" found in map config\n");
		exit(0);
	}
	else if(floorTile == '?') {
		printf("\nError: no tile named \"floor\" found in map config\n");
		exit(0);
	}
    
    // Initialize the map with walls
    std::vector<std::string> map(height, std::string(width, wallTile));

    // Set the random seed
    std::srand(seed);
    
    uint nrCells = width * height;	// total nr of cells in map
    uint carvedCells = 0;	// already carved out cells in map

	while(carvedCells < (nrCells * percentareWalls) / 100) {
		int startX, startY; 
		
		if(startcenter) {		// if startcenter is true, this will create a map with a large arena-style empty area in the middle
			startX = width / 2;
			startY = height / 2;
		}
		else {
			// Choose a random starting point
			startX = std::rand() % width;
			startY = std::rand() % height;
		}
		// Mark the starting point as a floor tile
		map[startY][startX] = floorTile;

		// Take random steps in different directions
		for (int i = 0; i < pathLength; i++) {
			int direction = getRandomDirection();

			// Move in the chosen direction
			switch (direction) {
				case 0: // up
					startY = std::max(startY - 1, 0);
					break;
				case 1: // down
					startY = std::min(startY + 1, height - 1);
					break;
				case 2: // left
					startX = std::max(startX - 1, 0);
					break;
				case 3: // right
					startX = std::min(startX + 1, width - 1);
					break;
			}
			// Mark the current cell as a floor tile
			if(map[startY][startX] == wallTile) {  // make it count as carved out only if it is still a wall
				map[startY][startX] = floorTile;
				carvedCells++;
			}
		}
	}
    return map;
}

int MapGenerator::getRandomDirection() {
    int direction = std::rand() % 4; // 0 = up, 1 = down, 2 = left, 3 = right
    return direction;
}
//// End of Drunkards walk map generator functions


std::vector<std::string> MapGenerator::PlaceEnemies(std::vector<std::string>& map, uint seed, const std::vector<MapData>* mapDataPtr){
	
	uint height = map.size();
	uint width = map[0].size();
	
	// Initialize the enemy map layee
    std::vector<std::string> enemy_layer(height, std::string(width, ' '));

    // Set the random seed
    std::srand(seed);
    
    uint startX, startY;
	bool tileBlocks = true;
	
	bool stairsUpfound = false;

	for(startY = 0; startY< height; startY++) {
		if(stairsUpfound) break; 	// we found the stairs in the inner loop, break out of the outer
		for(startX = 0; startX< width; startX++) {
			char stairsUp = cfg_getMapTileByName(*mapDataPtr, "stairsUp");
			if(map[startY][startX] == stairsUp) stairsUpfound = true;
			break;
		}
	}
	
	// this normally should not happen: there was no stairsUP symbol on the map
	// (maybe it is a manual map and the stairs were forgotten, or intentially
	// left out for testing purposes. instead of crashing the game, chose a 
	// random position
	if(!stairsUpfound) {
		do {
		// Choose a random starting point for player
		startX = std::rand() % width;
		startY = std::rand() % height;
		int tilePos = cfg_MapTileDataPos(map[startY][startX], mapDataPtr); // get tile at this position
		tileBlocks = (*mapDataPtr)[tilePos].blocks;		// check if it blocks movement		
		} while(tileBlocks);			// if so, try again (we don't want to spawn player in walls etc.)
	}
	
	enemy_layer[startY][startX] = '@';
	
	for(uint k=0; k<NR_ENEMIES; k++) {		// randomly place snakes in map
	
		bool tileBlocks = true;
		do {
		startX = std::rand() % width;
		startY = std::rand() % height;		
		int tilePos = cfg_MapTileDataPos(map[startY][startX], mapDataPtr); // get tile at this position
		tileBlocks = (*mapDataPtr)[tilePos].blocks;		// check if it blocks movement		
		} while(tileBlocks);			// if so, try again (we don't want to place anything in walls etc.)
	
		enemy_layer[startY][startX] = 'S';	
	}
	
	return enemy_layer;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Connect Isolated Areas and helper Functions
//
// the cellular Automata algorithm might generate map parts that are not accessible, because they are
// fully surrounded by walls. As we later absolutely do not want to spawn items or the player there,
// we have to connect those isolated areas
///////////////////////////////////////////////////////////////////////////////////////////////////////
void MapGenerator::connectIsolatedAreas(std::vector<std::string>& map, const std::vector<MapData>* mapDataPtr) {

	const char wallTile = cfg_getMapTileByName(*mapDataPtr, "wall");
    const char floorTile = cfg_getMapTileByName(*mapDataPtr, "floor");
    
    if(wallTile == '?'){
		printf("\nError: no tile named \"wall\" found in map config\n");
		exit(0);
	}
	else if(floorTile == '?') {
		printf("\nError: no tile named \"floor\" found in map config\n");
		exit(0);
	}
	
	// the following line is a bugfix: two areas might got connected by the borders of the map 
	// they would not be disconnected- but at the very end this method calls resetFloor and restores
	// the borders- potentially creating a new isolated area
    resetFloor(map, mapDataPtr);   // replace all flood and marker tiles with floor tile

    // first "flood" the map to see all connected floor tiles (mark them with '*')
    markConnectedAreas(map, floorTile, '*');

    int rows = map.size();
    int columns = map[0].size();

    // Find all unmarked floor tiles that are surrounded by walls
    std::vector<std::pair<int, int>> isolatedAreas = findIsolatedAreas(map, wallTile, floorTile);

    for (auto& pos : isolatedAreas) {
        int i = pos.first;
        int j = pos.second;

         map[i][j] = '-'; // mark outline of isolated areas, helped a lot during debugging this mess

        // Try to create path to nearest floor tile
        int minDist = rows + columns; // maximum possible distance in the map
        std::pair<int, int> nearestFloorTile = {-1, -1};

        // for each isolated area, find nearest flooded floor
        for (int k = 0; k < rows; k++) {
            for (int l = 0; l < columns; l++) {
                if (map[k][l] == '*') {
                    int dist = abs(k - i) + abs(l - j);
                    if (dist < minDist) {
                        minDist = dist;
                        nearestFloorTile = {k, l};
                    }
                }
            }
        }
        // now create the shortest path between the flooded and the isolated areas
        if (nearestFloorTile.first != -1) { // add path
            int k = nearestFloorTile.first;
            int l = nearestFloorTile.second;
            while (i != k || j != l) {
                if (i > k) {
                i--;
                } else if (i < k) {
                i++;
                } else if (j > l) {
                j--;
                } else if (j < l) {
                j++;
                }
                map[i][j] = '*';
            }
        }
    }
    resetFloor(map, mapDataPtr);   // replace all flood and marker tiles with floor tile
}


// fill adjacent floor tiles arount a tile if they are floor tiles
void MapGenerator::floodFill(std::vector<std::string>& map, int row, int col, char target, char replacement) {
    if (row < 0 || (uint) row >= map.size() || col < 0 || (uint) col >= map[0].size()) {
        return; // out of bounds
    }
    if (map[row][col] != target) {
        return; // not the target symbol
    }
    map[row][col] = replacement; // replace the symbol
    floodFill(map, row - 1, col, target, replacement); // top
    floodFill(map, row + 1, col, target, replacement); // bottom
    floodFill(map, row, col - 1, target, replacement); // left
    floodFill(map, row, col + 1, target, replacement); // right
}

// iteratively fill all connected areas with a marker tile (too see which parts of the map are isolated)
void MapGenerator::markConnectedAreas(std::vector<std::string>& map, char floorSymbol, char markSymbol) {
    // First mark all the floor symbols with the markSymbol
    for (uint i = 0; i < map.size(); ++i) {
        for (uint j = 0; j < map[0].size(); ++j) {
            if (map[i][j] == floorSymbol) {
                floodFill(map, i, j, floorSymbol, markSymbol);
                // Once we find one connected area, all the symbols in that area will be marked with markSymbol
                return; // exit early to avoid marking the entire map
            }
        }
    }
}

// iterates over each tile in the map and checks if the current tile is a floor tile and is
// surrounded by walls. If so, the tile's position is added to the isolatedAreas vector.
std::vector<std::pair<int, int>> MapGenerator::findIsolatedAreas(std::vector<std::string>& map, char floorTile, char wallTile) {
  int n = map.size();
  int m = map[0].size();
  std::vector<std::pair<int, int>> isolatedAreas;

  // Find all unmarked floor tiles that are surrounded by walls;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < m; j++) {
      if (map[i][j] == floorTile && (
            (i > 0 && map[i-1][j] == wallTile) ||
            (i < n-1 && map[i+1][j] == wallTile) ||
            (j > 0 && map[i][j-1] == wallTile) ||
            (j < m-1 && map[i][j+1] == wallTile)
          )) {
        isolatedAreas.push_back({i, j});
      }
    }
  }

  return isolatedAreas;
}

// After flooding and marking and connecting: reset all non-Wall tiles back to floor
void MapGenerator::resetFloor(std::vector<std::string>& map, const std::vector<MapData>* mapDataPtr) {
	
	const char floorTile = cfg_getMapTileByName(*mapDataPtr, "floor");
    if(floorTile == '?') {
		printf("\nError: no tile named \"floor\" found in map config\n");
		exit(0);
	}
	const char wallTile = cfg_getMapTileByName(*mapDataPtr, "wall");
    if(wallTile == '?') {
		printf("\nError: no tile named \"wall\" found in map config\n");
		exit(0);	
    }
		
	uint height = map.size();
	uint width = map[0].size();

	for (uint i = 0; i < height; i++) {
		for (uint j = 0; j < width; j++) {
			if (map[i][j] != wallTile) {
			map[i][j] = floorTile;
			}
		}
	}
	FixBorders(map, mapDataPtr);
}
// End of connect areas and helper functions


// Post processing methods
void MapGenerator::AddLakes(std::vector<std::string>& map, unsigned int seed, int numLakes, int minLakeSize, int maxLakeSize, const std::vector<MapData>* mapDataPtr) {
    
    const char waterTile = cfg_getMapTileByName(*mapDataPtr, "water");
    if(waterTile == '?') {
		printf("\nError: no tile named \"water\" found in map config\n");
		exit(0);
	}
    const char stairsUpTile = cfg_getMapTileByName(*mapDataPtr, "stairsUp");
    if(stairsUpTile == '?') {
		printf("\nError: no tile named \"stairsUp\" found in map config\n");
		exit(0);
	}
	
	const char stairsDownTile = cfg_getMapTileByName(*mapDataPtr, "stairsDown");
    if(stairsDownTile == '?') {
		printf("\nError: no tile named \"stairsDown\" found in map config\n");
		exit(0);	
    }
    
    std::srand(seed);

    int width = map[0].size();
    int height = map.size();

    for (int i = 0; i < numLakes; i++)
    {
        int x = std::rand() % width;
        int y = std::rand() % height;

        if(x == 0) x = 1;
        if(x == width) x = width - 1;
        if(y == 0) y = 1;
        if(y == height) y = height - 1;

        int Size = std::rand() % (maxLakeSize - minLakeSize + 1) + minLakeSize;

        for (int j = 1; j < Size; j++)
        {
            if ((map[y][x] != waterTile) && (map[y][x] != stairsUpTile) && (map[y][x] != stairsDownTile)) {
                map[y][x] = waterTile;
            }

            int direction = std::rand() % 4;

            switch (direction)
            {
            case 0:
                x++;
                break;
            case 1:
                x--;
                break;
            case 2:
                y++;
                break;
            case 3:
                y--;
                break;
            }

            if (x < 1 || x >= width-1 || y < 1 || y >= height-1)    // no water at the borders
            {
                break;
            }

            // Add some randomness to create curves and branches
            if (std::rand() % 3 == 0) {
                direction = std::rand() % 4;
            }
            // Check if the new coordinates are outside or at the border
            if (x+1 <= 0 || x+1 >= width-1 || y <= 0 || y >= height-1 ||
                x-1 <= 0 || x-1 >= width-1 || y <= 0 || y >= height-1 ||
                x <= 0 || x >= width-1 || y+1 <= 0 || y+1 >= height-1 ||
                x <= 0 || x >= width-1 || y-1 <= 0 || y-1 >= height-1)
            {
                break;
            }
        }
    }
}


void MapGenerator::AddMoss(std::vector<std::string>& map, unsigned int seed, uint mossWallProbability, uint mossWaterProbability, uint mossWaterWallProbability, const std::vector<MapData>* mapDataPtr)
{
	const char waterTile = cfg_getMapTileByName(*mapDataPtr, "water");
    if(waterTile == '?') {
		printf("\nError: no tile named \"water\" found in map config\n");
		exit(0);
	}
    const char floorTile = cfg_getMapTileByName(*mapDataPtr, "floor");
    if(floorTile == '?') {
		printf("\nError: no tile named \"floor\" found in map config\n");
		exit(0);
	}
	const char wallTile = cfg_getMapTileByName(*mapDataPtr, "wall");
    if(wallTile == '?') {
		printf("\nError: no tile named \"wall\" found in map config\n");
		exit(0);	
    }
	const char mossTile = cfg_getMapTileByName(*mapDataPtr, "moss");
    if(mossTile == '?') {
		printf("\nError: no tile named \"moss\" found in map config\n");
		exit(0);	
    }
	
    int width = map[0].size();
    int height = map.size();

    std::default_random_engine randGen(seed);
    std::uniform_real_distribution<float> mossChance(0.0f, 1.0f);
	
	float mossWallProb = (float) mossWallProbability / 100.0f;
	float mossWaterProb = (float) mossWaterProbability / 100.0f;
	float mossWaterWallProb = (float) mossWaterWallProbability /100.0f;
	
    for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {
            if (map[y][x] == floorTile) {
                bool hasWaterNearby = CountAdjacentWalls(map, width, height, x, y, waterTile) > 0;	// count how many water tiles are next to current tile
                bool hasWallNearby = CountAdjacentWalls(map, width, height, x, y, wallTile) >= 2;	// count how many wall tiles are next to current tile

                float mossRoll = mossChance(randGen);
                if (hasWaterNearby && mossRoll < mossWaterProb) {    // 60% chance of moss if floor tile is nearby water
                    map[y][x] = mossTile;
                }
                else if(hasWaterNearby && hasWallNearby && mossRoll < mossWaterWallProb) {   // 80% chance of moss if water AND wall next to it
                    map[y][x] = mossTile;
                }
                else if (hasWallNearby && mossRoll < mossWallProb) {    // 10% chance of moss if floor tile has walls next to it
                    map[y][x] = mossTile;
                }
            }
        }
    }
}



// This method uses a std::mt19937 random number generator with a given seed, as well as two
// std::bernoulli_distribution distributions for generating random boolean values with a given
// probability. It scans the map for areas with enough free space to place trees (i.e., at least
// a 3x3 block of floor tiles), and then places a tree with a certain probability in each such
// area. If a tree is placed, it also checks the neighboring tiles for additional trees with a certain probability
void MapGenerator::AddTrees(std::vector<std::string>& map, unsigned int seed, uint treeProbability, uint treeNeighbourProbability, const std::vector<MapData>* mapDataPtr)
{
	const char floorTile = cfg_getMapTileByName(*mapDataPtr, "floor");
    if(floorTile == '?') {
		printf("\nError: no tile named \"floor\" found in map config\n");
		exit(0);
	}
	const char treeTile = cfg_getMapTileByName(*mapDataPtr, "tree");
    if(treeTile == '?') {
		printf("\nError: no tile named \"tree\" found in map config\n");
		exit(0);	
    }
	
    int width = map[0].size();
    int height = map.size();

	float treeProb = (float) treeProbability / 100.0f;
	float treeNeighbourProb = (float) treeNeighbourProbability / 100.0f;

    std::mt19937 rng(seed);
    std::uniform_int_distribution<int> xDist(1, width - 2);
    std::uniform_int_distribution<int> yDist(1, height - 2);
    std::bernoulli_distribution treeDist(treeProb);      // probability of a tree within a free 3x3 area
    std::bernoulli_distribution neighborDist(treeNeighbourProb);  // probability to have a neighbor tree

    // Scan the map for areas with enough free space to place trees
    for (int y = 1; y < height - 1; y++) {
        for (int x = 1; x < width - 1; x++) {
            if (map[y][x] == floorTile) {
                // Check if there is enough free space to place a tree
                bool enoughSpace = true;
                for (int dy = -1; dy <= 1 && enoughSpace; dy++) {
                    for (int dx = -1; dx <= 1 && enoughSpace; dx++) {
                        enoughSpace = (map[y + dy][x + dx] == floorTile);
                    }
                }
                // Place a tree with a certain probability
                if (enoughSpace && treeDist(rng)) {
                    map[y][x] = treeTile;
                    // Check if a neighboring tile should also have a tree
                    for (int dy = -1; dy <= 1; dy++) {
                        for (int dx = -1; dx <= 1; dx++) {
                            if ((dx != 0 || dy != 0) && map[y + dy][x + dx] == floorTile && neighborDist(rng)) {
                                map[y + dy][x + dx] = treeTile;
                            }
                        }
                    }
                }
            }
        }
    }
}

void MapGenerator::AddGates(std::vector<std::string>& map, unsigned int seed, uint wallReplacementProb, uint wallWaterReplacementProb, const std::vector<MapData>* mapDataPtr)
{
	const char waterTile = cfg_getMapTileByName(*mapDataPtr, "water");
    if(waterTile == '?') {
		printf("\nError: no tile named \"water\" found in map config\n");
		exit(0);
	}
    const char floorTile = cfg_getMapTileByName(*mapDataPtr, "floor");
    if(floorTile == '?') {
		printf("\nError: no tile named \"floor\" found in map config\n");
		exit(0);
	}
	const char wallTile = cfg_getMapTileByName(*mapDataPtr, "wall");
    if(wallTile == '?') {
		printf("\nError: no tile named \"wall\" found in map config\n");
		exit(0);	
    }
	const char gateTile = cfg_getMapTileByName(*mapDataPtr, "fence");
    if(gateTile == '?') {
		printf("\nError: no tile named \"fence\" found in map config\n");
		exit(0);	
    }
	
    int width = map[0].size();
    int height = map.size();

    srand(seed); // Seed the random number generator

    // Replace walls with '+' symbols
    for (int y = 1; y < height - 1; y++)
    {
        for (int x = 1; x < width - 1; x++)
        {
            if (map[y][x] == wallTile)	// only existing wall tiles should be replaced, otherwise we run the risk of creating new isolated areas
            {
                if (((map[y][x-1] == floorTile && map[y][x+1] == floorTile) ||   // if wall tile is surrounded by floor tiles horizontally or vertically
                    (map[y-1][x] == floorTile && map[y+1][x] == floorTile)) ||
                   ((map[y][x-1] == gateTile && map[y][x+1] == gateTile) ||   // or if wall tile is surrounded by gate tiles horizontally or vertically
                    (map[y-1][x] == gateTile && map[y+1][x] == gateTile)))
                {
                    if (std::rand() % 100 < (int)wallReplacementProb)           // replace wall tile with gate tile
                    {
                        map[y][x] = gateTile;
                    }
                }

                if ((map[y][x-1] == waterTile || map[y][x+1] == waterTile) ||   // if wall tile is adjacent to water tiles
                    (map[y-1][x] == waterTile || map[y+1][x] == waterTile))
                {
                    if (std::rand() % 100 < (int) wallWaterReplacementProb)            // replace wall tile with gate tile
                    {
                        map[y][x] = gateTile;
                    }
                }
            }
        }
    }
	
	// ToDo: temporary bugfix- this code can lead to isolated areas
    // Replace walls adjacent to '.' symbols with '+'
    //for (int y = 1; y < height - 1; y++)
    //{
        //for (int x = 1; x < width - 1; x++)
        //{
            //if (map[y][x] == floorTile)
            //{
                //if (CountAdjacentWalls(map, width, height, x, y, gateTile) > 0)
                //{
                    //if (std::rand() % 100 < (int) wallReplacementProb)
                    //{
                        //map[y][x] = gateTile;
                    //}
                //}
            //}
        //}
    //}
}

// restore borders that might have got overwritten during post processing
void MapGenerator::FixBorders(std::vector<std::string>& map, const std::vector<MapData>* mapDataPtr){
	
	const char wallTile = cfg_getMapTileByName(*mapDataPtr, "wall");
    if(wallTile == '?') {
		printf("\nError: no tile named \"wall\" found in map config\n");
		exit(0);	
    }
	uint width = map[0].size();
    uint height = map.size();
	
	// ensure all 4 borders of the map are walls
	for(uint i = 0; i < height; i++){
		map[i][0] = wallTile;
		map[i][width-1] = wallTile;
	}
	 for(uint j = 0; j < width; j++){
		map[0][j] = wallTile;
		map[height-1][j] = wallTile;
	}
}


void MapGenerator::PlaceObjects(std::vector<std::string>& map, uint seed, const std::vector<MapData>* mapDataPtr){
	
	uint height = map.size();
	uint width = map[0].size();
	
	Dijkstra dijkstra; 
	
	// TODO: the following is a workaround: the Dijkstra methods expect an existing tileInfo matrix 
	// which is generated in Level.cpp while reading the map. The MapGenerator methods don't have this
	// so we have to generate a temporary tileInfo vector from the map
    std::vector<std::vector<TileInfo>> tileInfo;
    for (uint y = 0; y < height; y++) {
        std::vector<TileInfo> row;
        for (uint x = 0; x < width; x++) {
            TileInfo currentTile;
            int tilePos = cfg_MapTileDataPos(map[y][x], mapDataPtr); // get tile at this position
            bool tileBlocks = (*mapDataPtr)[tilePos].blocks;		// check if it blocks movement
            if(tileBlocks) {
				currentTile.blocks = true;
			}
			else {
				currentTile.blocks = false;
			}
            row.push_back(currentTile);
        }
        tileInfo.push_back(row);
    }
	
	// Set the random seed
    std::srand(seed);
    
    uint startX, startY;
	
	bool tileBlocks = true;
	
	do {
		// Choose a random starting point for player
		startX = std::rand() % width;
		startY = std::rand() % height;		
		
		int tilePos = cfg_MapTileDataPos(map[startY][startX], mapDataPtr); // get tile at this position
		tileBlocks = (*mapDataPtr)[tilePos].blocks;		// check if it blocks movement
	} while (tileBlocks);	// if so, try again (we don't want to place anything in walls etc.)
	
	// TODO: hardcoded symbols!
	map[startY][startX] = '<';
	
	LevelPoint endLevel = dijkstra.FindFarthestTile(&tileInfo, (LevelPoint) {startX,startY});
	map[endLevel.Y][endLevel.X] = '>'; 		// place the stairs down symbol as far away as possible
	
	tileInfo.clear();	// delete temporary tileInfo matrix
}
