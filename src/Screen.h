#pragma once
#include <ncurses.h>

class Screen{
public:
    Screen();   // Initialize ncurses
    ~Screen();  // clear ncurses

    void add(const char *message);  // Print message to screen

    // Setters

    // Getters
    int getHeight() const;
    int getWidth() const;

private:
    int m_height, m_width;
};
