#pragma once
#include <string>
#include <vector>
#include "Datatypes.h"
#include "TimedEntities.h"
// Base class for all characters, whether they are player, enemy or NPC

class Character: public TimedEntity{
protected:
    Character(char tile, std::string name, uint level, int health, uint attack, uint defense, uint visRange, std::vector<std::string> descriptions, std::vector<std::string> battleCries, uint speed);
    ~Character();
    
public:  
    // Getters
    void getPosition(LevelPoint &pos) const;
    std::string getName() const;
    char getTile() const;
    uint getLevel() const;
    uint getAttack() const;
    uint getDefense() const;
    int getHealth() const;
    uint getVisRange() const;
    std::vector<std::vector<FoVInfo>>* getFoVPtr();	// get handle for FoV matrix
    std::vector<LevelPoint>* getMoveQueuePtr() const;	// get handle for movement Queue vector
    bool isInFoV(LevelPoint &pos) const; 		// given position is visible for character
	bool getWasSeenPlayer() const;
	bool getHasSeenPlayer() const;
	std::string getRandomDescription() const;	// return randomly one of the descriptions of the character
	std::string getBattleCry() const; 	// return randomly one of the battle cries of the monster
	EntityState getState() const;
	uint getBasicSpeed() const;		// get basic speed (before all modifiers) of character
	
	
	virtual int takeTurn(EntityAction action = EntityAction::IDLE) = 0;	// pure virtual function
	
    // Setters
    void setPosition(LevelPoint pos);
    void setVisRange(uint range);
    void setMoveQueue(const std::vector<LevelPoint>& newMoveQueue);
    void setWasSeenPlayer(bool wasSeen);
    void setHasSeenPlayer(bool hasSeen);
    void setState(EntityState newstate);
    void setBasicSpeed(uint basicSpeed);	// set basic character speed (before applying modifiers)
    void setFoVPtr(const std::vector<std::vector<FoVInfo>>& fovInfo); 	// set handle for the FoV matrix
    
protected:
    // Character Properties
    char m_tile;   
    std::string m_name;
    uint m_level;
    int m_health;
    uint m_attack;
    uint m_defense;
    uint m_visibilityRange;
    uint m_basicSpeed;		// basic speed as configured (before all modifiers)
    
    EntityState m_state = ALIVE;
    
    std::vector<std::string> m_descriptions;
    std::vector<std::string> m_battleCries;
    
    bool m_hasSeenPlayer = false;
    bool m_wasSeenByPlayer = false;
    
    // Position
    LevelPoint m_position;
    std::vector<std::vector<FoVInfo>> m_fovInfo; // Holds FoV data for each character, for each tile
    
    // Navigation
    std::vector<LevelPoint>* m_moveQueue; // vector that can contain target tiles for movement
};
