#include <iostream>
#include <deque>

#include "Datatypes.h"
#include "Debug.h"
#include "getchar.h"
#include "GameSystem.h"
#include "Interface.h"
#include "Visibility.h"
#include "Dijkstra.h"
#include "GameClock.h"

extern GameConfig gameConfig;

// Constructor, sets up the game
GameSystem::GameSystem(const std::vector<EnemyData>* enemiesConfig, const std::vector<MapData>* mapTilesConfig) :
    m_clock(std::make_shared<GameClock>()),
    m_levelList()
{
	// Set the map seed
    std::mt19937 rng(gameConfig.seed);
    generateLevel(enemiesConfig, mapTilesConfig);
}

// Destructor
GameSystem::~GameSystem(){
	delete m_player;
}

// creates configured nr of levels and appends them to m_levelList
void GameSystem::generateLevel(const std::vector<EnemyData>* enemiesConfig, const std::vector<MapData>* mapTilesConfig) {
    std::mt19937 rng(gameConfig.seed);

    for (uint levelNr = 0; levelNr < gameConfig.nrLevels; levelNr++) {
		
		std::unique_ptr<Level> level = std::make_unique<Level>(m_clock);	// add game clock to level
		// branch for procedural or hand made level
		if(gameConfig.levelNames[levelNr] == "random") {
			uint mapSeed = rng();
			generateProceduralMap(mapSeed, level, enemiesConfig, mapTilesConfig);
		}
		else {
			level->loadLevel(gameConfig.levelNames[levelNr]);  
			level->processLevel(m_player, enemiesConfig, mapTilesConfig);   
		}
		// add level to level rotation
		m_levelList.push_back(std::move(level));
	}
	
	// init the interface for the first map
	interface.init(m_levelList[0]->getMapSize());
	m_levelList[0]->updateMap(interface.map, m_player->getFoVPtr());
	m_levelList[0]->setInterface(&interface);
}

// creates a map procedurally
void GameSystem::generateProceduralMap(uint seed, std::unique_ptr<Level>& level, const std::vector<EnemyData>* enemiesConfig, const std::vector<MapData>* mapTilesConfig){
	
	auto map = m_mapGenerator.CellularAutomata(seed,
								gameConfig.mapWidth, 
								gameConfig.mapHeight, 
								gameConfig.CellAutIterations, 
								gameConfig.CellAutPercentWalls,  
								mapTilesConfig);
	
	m_mapGenerator.connectIsolatedAreas(map, mapTilesConfig);
	
	m_mapGenerator.PlaceObjects(map, seed, mapTilesConfig);
	
	auto enemyLayer = m_mapGenerator.PlaceEnemies(map, seed, 
												mapTilesConfig);
																			
	m_mapGenerator.AddLakes(map, seed, gameConfig.nrLakes, gameConfig.minLakeSize, gameConfig.maxLakeSize, mapTilesConfig);
    m_mapGenerator.AddMoss(map, seed, gameConfig.mossNextWallProb, gameConfig.mossNextWaterProb, gameConfig.mossNextWaterWallProb, mapTilesConfig);
    m_mapGenerator.AddTrees(map, seed, gameConfig.treeProb, gameConfig.treeNeighbourProb, mapTilesConfig);
    m_mapGenerator.AddGates(map, seed, gameConfig.gateWallProb, gameConfig.gateWaterProb, mapTilesConfig);
    m_mapGenerator.FixBorders(map, mapTilesConfig);
	
	level->setUpMap(map, mapTilesConfig);
	level->setUpEnemies(m_player, enemiesConfig, enemyLayer);
}


void GameSystem::playGame(){
    Dijkstra pathfinder;	// setup path finding algorithm
    
    m_levelIdx = 0; 	// start with the first level
    
    LevelPoint playerPos;     
    playerPos = m_levelList[0]->getLadderUpPosition();	// get initial position
    m_player->setPosition(playerPos);
    
    interface.mapWin.center(playerPos.X, playerPos.Y);
    interface.mapWin.refresh();
    
    RayCastVisibility visibility(m_levelList[m_levelIdx].get());
    RayCastVisibility enemy_vis(m_levelList[m_levelIdx].get());
    
    std::vector<std::vector<FoVInfo>>* fovInfoPtr = m_player->getFoVPtr();	// get handle for player visibility
    
    if(gameConfig.playerFoV) {
		visibility.Compute(fovInfoPtr, playerPos, m_player->m_visibilityRange); 	// and calculate what player sees at start 
	}
	else {
		visibility.SetAllVisible(fovInfoPtr);
	}
	
	interface.mapWin.center(playerPos.X, playerPos.Y);
    interface.mapWin.refresh();
    
	//////////////////////////////////////////////////////////////////////////////
	// End of init stuff- Let's Go!
	//////////////////////////////////////////////////////////////////////////////
    interface.addGameLog("You wake up in a dungeon. You have no equipment.");
    interface.PLAYER_HEARS("movements in the darkness");
    
    bool isDone = false;
    
    bool firstMoveInLevel = true; 	// indicates if player already has moved in this level
      
    // Main Game Loop
    while(isDone != true){
        interface.updatePlayerStatus(m_player->getLevel(),m_player->getHealth(),
                                    m_player->getAttack(),m_player->getDefense(),
                                    m_player->getExperience());
        
        interface.clearWin(interface.map);
        m_levelList[m_levelIdx]->updateMap(interface.map, m_player->getFoVPtr());
        interface.refreshAll();
        
        // recalculate FoV for all enemies (left)
        for(uint i=0; i< m_levelList[m_levelIdx]->getNrEnemies(); i++) {
			std::shared_ptr<Enemy>* ppEnemy = m_levelList[m_levelIdx]->getEnemy(i);  // get enemy handle from list
			std::shared_ptr<Enemy> pEnemy = *ppEnemy;              // obtain shared pointer to Enemy object
			Enemy* enemy = pEnemy.get();                           // obtain raw Enemy* pointer

			LevelPoint enemyPos;	
			enemy->getPosition(enemyPos);	// get currents enemy position
			std::vector<std::vector<FoVInfo>>* enemyFov = enemy->getFoVPtr();	// get FoV handle for this enemy
			enemy_vis.Compute(enemyFov, enemyPos, enemy->getVisRange());	// update this enemy´s FoV
			//LOG_ENGINE_DEBUG("E: %i state: %i (%i,%i) act pts: %i speed: %i", i, enemy->getState(), enemyPos.X, enemyPos.Y, enemy->getActionPoints(), enemy->getSpeed());
		}
        LOG_ENGINE_DEBUG("P:___state: %i (%i,%i) act pts: %i speed: %i___LvlIdx: %i", m_player->getState(), m_player->m_position.X, m_player->m_position.Y, m_player->getActionPoints(), m_player->getSpeed(), m_levelIdx);
        
        // Call tick() on all timed entities
		for (auto& entity : m_levelList[m_levelIdx]->getTimedEntitiesPtr()) {
			entity->tick();
		}
		
        if(!playerMove()) {
			isDone = true;
		}
     
		checkForLevelSwitch(playerPos, visibility, enemy_vis, firstMoveInLevel);

		m_levelList[m_levelIdx]->updateEnemies(m_player);
	}
}

// executes players command, returns true if game continues, false if player chooses to end game
bool GameSystem::playerMove(){
    char input;
	
	const std::vector<std::vector<TileInfo>>* tileInfo;
	LevelPoint playerPos = (LevelPoint){0,0};
	
	tileInfo = m_levelList[m_levelIdx]->getTileInfo();
	m_player->getPosition(playerPos);
	std::string tileDesc = (*tileInfo)[playerPos.Y][playerPos.X].description;
	
	if((*tileInfo)[playerPos.Y][playerPos.X].name == "stairsDown") {
		LOG_ENGINE_DEBUG("You've hit the stairs down");
	}
	
    interface.clearWin(interface.commandWin);
    interface.addMsg(interface.commandWin, "%s\n", tileDesc.c_str());
    interface.addMsg(interface.commandWin, "Enter a move command (w/a/s/d)		x to exit\n");
    interface.addMsg(interface.commandWin, "Level: %i\t  Round: %i\t      Time: %s", m_levelIdx+1, m_clock->getNrRounds(), m_clock->getTimeString().c_str());
    
    interface.refreshAll();
    input = get_che();
	
	if(input == 'x'){		// ESC key pressed
		endGame();
		return false;
	}
	
    waddch(interface.commandWin.getWin(), input);
    m_levelList[m_levelIdx]->movePlayer(input, m_player, interface);
    
    return true;
}

void GameSystem::endGame(){

}

bool GameSystem::checkForLevelSwitch(LevelPoint &playerPos, RayCastVisibility& playerVisibility, RayCastVisibility& enemyVisibility, bool &firstMove) {
	bool gameCompleted = false;
	
	std::vector<std::vector<FoVInfo>>* fovInfoPtr = m_player->getFoVPtr();	// get handle for player visibility
	
	m_player->getPosition(playerPos);    
	
	const std::vector<std::vector<TileInfo>>* tilePtr = m_levelList[m_levelIdx]->getTileInfo();
	
	if( ((*tilePtr)[playerPos.Y][playerPos.X].name == "stairsDown") && !firstMove) {
			LOG_ENGINE_DEBUG("switched to Level %i", m_levelIdx+1);
		if((m_levelIdx + 1) >= (int) m_levelList.size()){
			gameCompleted = true;		// player reached last level, end game
		}
		else {
			switchLevel(m_levelIdx, playerVisibility, enemyVisibility, true);
			firstMove = true;
		}
	} 
	else if( ((*tilePtr)[playerPos.Y][playerPos.X].name == "stairsUp") && !firstMove){
		if((m_levelIdx -1 ) < 0) {
			interface.addGameLog("You do not dare to return empty-handed");
		}
		else {
			switchLevel(m_levelIdx, playerVisibility, enemyVisibility, false);
			firstMove = true;
		}
	}
	else {
		if(gameConfig.playerFoV) {    	
			playerVisibility.Compute(fovInfoPtr, playerPos, m_player->m_visibilityRange);
			firstMove = false; 	// we now know that player has moved in level
		}

		interface.mapWin.center(playerPos.X, playerPos.Y);
		interface.mapWin.refresh();
	}
	
	return gameCompleted;
}

void GameSystem::switchLevel(const uint lvlNr, RayCastVisibility& playerVisibility, RayCastVisibility& enemyVisibility, bool levelDown) {
	LevelPoint playerPos;
	m_player->getPosition(playerPos);
	const std::vector<std::vector<TileInfo>>* tilePtr = m_levelList[m_levelIdx]->getTileInfo();
	
	interface.clearWin(interface.commandWin);
	interface.addMsg(interface.commandWin, "%s\n", (*tilePtr)[playerPos.Y][playerPos.X].description.c_str());
	if(levelDown) {
		interface.addMsg(interface.commandWin, "really want to descend down (y/n)		\n");
	}
	else {
		interface.addMsg(interface.commandWin, "really want to ascend up (y/n)		\n");
	}
    
    interface.refreshAll();
	char input = get_che();
	
	if(input == 'n'){		// exit routine if player chooses to stay in level
		return;
	}

	std::vector<std::vector<FoVInfo>>* fovInfoPtr = m_player->getFoVPtr();	// get handle for player visibility
   
   interface.clearWin(interface.map);	// clear the map
   
	if(gameConfig.playerFoV && levelDown) {
		// switch visibility pointers: store the visibility of the explored level in Level´s m_playerFoVInfo
		// then switch the visibility matrix of the player to the current´s level visibility
		// this way we can revisit already explored levels and start with the discovered map
		m_levelList[m_levelIdx]->setPlayerFoVInfo(*fovInfoPtr);
		m_player->setFoVPtr(*(m_levelList[m_levelIdx + 1]->getFoVPtr()));
		
		// set the starting position to where the ladder up symbol is located
		m_player->setPosition(m_levelList[m_levelIdx + 1]->getLadderUpPosition());
		m_levelIdx++;
	}
	else if(gameConfig.playerFoV && !levelDown) {
		// same, but reversed logic to the FoV switch in case of levelDown
		m_levelList[m_levelIdx]->setPlayerFoVInfo(*(m_player->getFoVPtr()));
		m_player->setFoVPtr(*(m_levelList[m_levelIdx - 1]->getFoVPtr()));
		
		// set the starting position to where the ladder Down symbol is located
		m_player->setPosition(m_levelList[m_levelIdx - 1]->getLadderDownPosition());
		m_levelIdx--;
	}

	// reinitialize interface because map size might be different
	interface.init(m_levelList[m_levelIdx]->getMapSize());
	m_levelList[m_levelIdx]->updateMap(interface.map, m_player->getFoVPtr());
	m_levelList[m_levelIdx]->setInterface(&interface);

	// update map window and center on player
	m_player->getPosition(playerPos);
	interface.clearWin(interface.map);	// clear the map
	interface.mapWin.center(playerPos.X, playerPos.Y);
	interface.mapWin.refresh();

	// get FoV handles for current levels (if already visited, this ensures we start with what was already discovered
	playerVisibility = RayCastVisibility(m_levelList[m_levelIdx].get());
	enemyVisibility = RayCastVisibility(m_levelList[m_levelIdx].get());

	playerVisibility.Compute(fovInfoPtr, playerPos, m_player->m_visibilityRange); 	// calculate what player sees
}
