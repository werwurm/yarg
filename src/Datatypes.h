#pragma once
#include <algorithm>
#include <string>

struct TileInfo {       // amplifying Tile Information
	bool blocks;		// Holds information if tile blocks movement
    bool translucent;   // blocks light (false/0) / is translucent (true/ 1)
    uint movementCost;	// the cost to move to this tile (for pathfinding)
    std::string name;   // tile name
    std::string description; 	// a one liner description
    
    struct BattleModifiers {	// how the tile affects battle
		float attack = 1.0;
		float defence = 1.0;
	} battleModifiers;
}; 

struct FoVInfo {        // map tile information wrt to field of view
    bool visible;
    bool discovered;
};

struct MapSize{
    unsigned int Width;
    unsigned int Height;  
};

struct LevelPoint{      // 2D point in game world/ map
    unsigned int X;
    unsigned int Y;
    
    bool operator==(const LevelPoint& other) const {
        return X == other.X && Y == other.Y;
    }

    bool operator!=(const LevelPoint& other) const {
        return !(*this == other);
    }
};

enum EntityAction {
	IDLE,
	MOVE,
	ATTACK,
};

enum EntityState {
	ALIVE,
	DEAD,
};
