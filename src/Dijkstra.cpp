#include <algorithm>
#include <climits>
#include <vector>
#include <queue>
#include <climits>
#include <cmath>

#include "Dijkstra.h"
#include "Datatypes.h"

/*
* based on code given by ChatGPT
* The Dijkstra class implements Dijkstra's algorithm for finding the shortest path in a 2D 
* grid. Each TileInfo object contains information about the movement cost and whether the 
* tile blocks movement.
* The FindPath method takes in a pointer to the 2D vector of TileInfo objects, as well as 
* the starting and target points and returns a vector of LevelPoint objects representing the
* shortest path from the starting point to the target point.
* The method first initializes the distances from the starting point to all other points
* with a maximum value (in this case, INT_MAX). It then initializes a priority queue using 
* a custom comparator function called CompareLevelPoint.
* 
* The method uses Dijkstra's algorithm to explore the grid in a breadth-first manner, starting
* from the start point. It visits neighboring points, calculates their distances from the start 
* point, and updates the distances and previous points accordingly. It continues this process 
* until the target point is reached or all reachable points have been visited.
* 
* After the algorithm completes, the method reconstructs the shortest path from the start to
* the target point by following the previous points recorded during the algorithm's execution. 

* Note: The CompareLevelPoint comparator function is defined as a lambda function inside the 
* FindPath method, and it compares LevelPoint objects based on their distances from the start 
* point, which is calculated using the distances vector.
*/

Dijkstra::Dijkstra() {}

std::vector<LevelPoint> Dijkstra::FindPath(const std::vector<std::vector<TileInfo>>* tileInfo, const LevelPoint start, const LevelPoint target) {
    // Initialize distances with maximum value
    std::vector<std::vector<int>> distances(tileInfo->size(), std::vector<int>((*tileInfo)[0].size(), INT_MAX));
    distances[start.Y][start.X] = 0;  // Distance from start to itself is 0

    // Initialize priority queue for Dijkstra's algorithm with custom comparator
    auto CompareLevelPoint = [&](const LevelPoint& lhs, const LevelPoint& rhs) {
        return distances[lhs.Y][lhs.X] > distances[rhs.Y][rhs.X];
    };
    std::priority_queue<LevelPoint, std::vector<LevelPoint>, decltype(CompareLevelPoint)> pq(CompareLevelPoint);
    pq.push(start);

    // Initialize previous points for reconstructing the path
    std::vector<std::vector<LevelPoint>> previous(tileInfo->size(), std::vector<LevelPoint>((*tileInfo)[0].size(), {UINT_MAX, UINT_MAX}));
    previous[start.Y][start.X] = start;

    // Dijkstra's algorithm
    while (!pq.empty()) {
        // Get the point with the smallest distance
        LevelPoint current = pq.top();
        pq.pop();

        // Stop if we've reached the target
        if (current.X == target.X && current.Y == target.Y) {
            break;
        }

        // Visit neighbors (4 directions)
        for (int dx = -1; dx <= 1; ++dx) {
            for (int dy = -1; dy <= 1; ++dy) {
                if (abs(dx) + abs(dy) == 1) {  // Restrict movement to 4 directions
                    int newX = current.X + dx;
                    int newY = current.Y + dy;
                    //if ( (newX >= 0) && (newX < (int) tileInfo->size()) && (newY >= 0) && (newY < (int)(*tileInfo)[0].size())) 
                    if ( (newY >= 0) && (newY < (int) tileInfo->size()) && (newX >= 0) && (newX < (int)(*tileInfo)[0].size())) {
                        int newDistance = distances[current.Y][current.X] + (*tileInfo)[newY][newX].movementCost;
                        if (!(*tileInfo)[newY][newX].blocks && newDistance < distances[newY][newX]) {
                            distances[newY][newX] = newDistance;
                            previous[newY][newX] = current;
                            pq.push({(unsigned int)newX, (unsigned int)newY});
                        }
                    }
                }
            }
        }
    }

    // Reconstruct the path from start to target
    std::vector<LevelPoint> path;
    if (previous[target.Y][target.X].X != UINT_MAX && previous[target.Y][target.X].Y != UINT_MAX) {
        LevelPoint current = target;
        while (current.X != start.X || current.Y != start.Y) {
            path.push_back(current);
            current = previous[current.Y][current.X];
        }
        path.push_back(start);
        std::reverse(path.begin(), path.end());  // Reverse the path to get correct order
    }

    return path;
}

// Compute the distances from the starting tile to all reachable tiles using Dijkstra's algorithm.
// Returns a 2D vector of distances.
std::vector<std::vector<int>> Dijkstra::ComputeDistances(const std::vector<std::vector<TileInfo>>* tileInfo, const LevelPoint& start) {
    std::vector<std::vector<int>> distances(tileInfo->size(), std::vector<int>(tileInfo->at(0).size(), -1));  // initialize all distances to -1 (unreachable)
    std::queue<LevelPoint> q;
    q.push(start);
    distances[start.Y][start.X] = 0;  // distance to starting tile is 0
    while (!q.empty()) {
        LevelPoint curr = q.front();
        q.pop();
        for (const auto& neighbor : GetNeighbors(tileInfo, curr)) {  // get all neighbors that allow movement
            if (distances[neighbor.Y][neighbor.X] == -1) {  // if neighbor is not yet reached
                std::vector<LevelPoint> path = Dijkstra::FindPath(tileInfo, start, neighbor);  // compute shortest path from start to neighbor
                if (path.empty()) continue;  // if no path found, skip this neighbor
                int distance = static_cast<int>(path.size()) - 1;  // compute distance to neighbor (number of steps)
                distances[neighbor.Y][neighbor.X] = distance;
                q.push(neighbor);  // add neighbor to queue to explore its neighbors in the next iteration
            }
        }
    }
    return distances;
}


// Find the tile that is farthest away from the starting tile and allows movement.
// Returns the farthest tile as a LevelPoint.
LevelPoint Dijkstra::FindFarthestTile(const std::vector<std::vector<TileInfo>>* tileInfo, const LevelPoint& start) {
    std::vector<std::vector<int>> distances = ComputeDistances(tileInfo, start);  // compute distances from starting tile
    int maxDistance = -1;  // initialize maximum distance to -1 (no tile reached yet)
    LevelPoint farthestTile{0, 0};  // initialize farthest tile to an invalid tile
    
    for (uint y = 0; y < distances.size(); ++y) {
        for (uint x = 0; x < distances[0].size(); ++x) {
            if (!(*tileInfo)[y][x].blocks && distances[y][x] > maxDistance) {  // if tile allows movement and has a greater distance than the current maximum
                maxDistance = distances[y][x];
                farthestTile = (LevelPoint) {x, y};
            }
        }
    }
    return farthestTile;
}

// Returns a vector of neighboring tiles that allow movement, given a 2D vector of tile information and a current position
std::vector<LevelPoint> Dijkstra::GetNeighbors(const std::vector<std::vector<TileInfo>>* tileInfo, const LevelPoint& curr) {
    std::vector<LevelPoint> neighbors;

    // Check the tile above the current position
    if (curr.Y > 0 && !(*tileInfo)[curr.Y-1][curr.X].blocks) {
        neighbors.push_back((LevelPoint){curr.X, curr.Y-1});
    }

    // Check the tile below the current position
    if (curr.Y < (tileInfo->size()-1) && !(*tileInfo)[curr.Y+1][curr.X].blocks) {
        neighbors.push_back((LevelPoint){curr.X, curr.Y+1});
    }

    // Check the tile to the left of the current position
    if (curr.X > 0 && !(*tileInfo)[curr.Y][curr.X-1].blocks) {
        neighbors.push_back((LevelPoint){curr.X-1, curr.Y});
    }

    // Check the tile to the right of the current position
    if (curr.X < ((*tileInfo)[0].size()-1) && !(*tileInfo)[curr.Y][curr.X+1].blocks) {
        neighbors.push_back((LevelPoint){curr.X+1, curr.Y});
    }

    return neighbors;
}

